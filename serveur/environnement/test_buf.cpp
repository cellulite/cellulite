#include <iostream>
#include <string>
#include "buffer.h"
using namespace std;

int main()
{
	Buffer buf;
	Message msgs1[8] = { "Salut,", "tu" , "vas", "bien", "?" };
	Message msgs2[8] = { "Je", "vais", "bien", "et", "toi", "?", "Ca", "marche !!!!!!!!!" };
	for(int i = 0 ; i < 8 ; ++i)
	  buf.store(msgs1[i], (Direction)i); // Ce cast est obligatoire !!
	cout << buf << endl;
	buf.update();
	cout << buf << endl;
	for(int i = 0 ; i < 8 ; ++i)
	  buf.store(msgs2[i], (Direction)i);
	cout << buf << endl;
	buf.update();
	string mess1 = "Un petit test: il n'y a pas d'autre message que celui-ci et pourtant je vais lire le message venant de 1!";
	buf.store(mess1, (Direction)0);
	cout << buf << endl;
	buf.update();
	cout << buf << endl;
	cout << "Je vais lire le message venant de 0" << endl;
	cout << "buf.load((Direction)0); : " << buf.load((Direction)0) << endl;
	cout << "Je vais lire le message venant de 1 (il n'y a rien):" << endl;
	cout << "buf.load((Direction)1); : " << buf.load((Direction)1) << endl;
	cout << "size(buf.load((Direction)1)).size(); : " << (buf.load((Direction)1)).size() << endl;
	return 0;
}
