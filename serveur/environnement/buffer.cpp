#include <cstdlib>
#include <csignal>
#include <iostream>
#include "buffer.h"
using namespace std;

Buffer::Buffer()
{
	data_temp = new Message [8];
	data = new Message [8];
}

Buffer::~Buffer()
{
	delete[] data_temp;
	delete[] data;
}

Message Buffer::load(Direction dir)
{
	return data[dir];
}

Buffer& Buffer::store(const Message msg, Direction dir)
{
  data_temp[dir] = msg;
  return *this;
}

Buffer& Buffer::update()
{
	delete[] data;
	data = data_temp;
	data_temp = new Message [8];
	return *this;
}

ostream& operator<< (ostream& output, const Buffer& buf)
{
	for(int i = 0 ; i < 8 ; ++i)
	{
		output << buf.data[i] << " <-- " << buf.data_temp[i] << endl;
	}
	return output;
}
