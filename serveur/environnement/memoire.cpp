#include <cstdlib>
#include <csignal>
#include <iostream>
#include <fstream>
#include <limits.h>
#include "memoire.h"
#include "Log.h"
using namespace std;

// -- Objets pour logger les erreurs déclarés dans serveur.cpp: -- //
extern Log log_er;
extern fstream filestr;

Memoire::Memoire()
: p_nb_vars	(0)
, data		(NULL)
{
}

Memoire::~Memoire()
{
	delete[] data;		// on freeze la structure, il faut car peut-être qu'on a lancé set_taille (avec son new int)
}

Memoire& Memoire::set_taille(int nb_vars_)
{
	if(data)
	{
		log_er << "Erreur dans Memoire::set_taille : Memoire non vide\n";
		raise(SIGILL);
		return *this;
	}
	else			// ouai ok ... ^^
	  {
	    p_nb_vars = nb_vars_;
	    data = new int [p_nb_vars]; // on alloue de la mémoire pour stocker data
	    for (int i = 0; i<p_nb_vars; i++)
	      {
		data[i] = INT_MIN;
	      }
	    return *this;
	  }
}

int& Memoire::operator[] (const int indice_)
{
  // on gère les erreurs (noter que si set_taille n'a pas été appelé alors cette condition
  // n'est jamais vérifié car p_nb_vars est initialisé à 0).
  if(0 <= indice_ && indice_ < p_nb_vars)
    return data[indice_];
  log_er << "Erreur dans Memoire::operator[] : indice = " << indice_ 
       << " alors que p_nb_vars = " << p_nb_vars
       << "\nSegfault en vue ^^\n";
  raise(SIGSEGV);
  return data[0];
}

int Memoire::nb_vars () const { return p_nb_vars; };

ostream& operator<< (ostream& output, const Memoire& mem)
{
	int limit = mem.p_nb_vars;
	output << "[";
	for(int i = 0 ; i < limit - 1 ; ++i)
	{
		output << mem.data[i] << " ";
	}
	if(limit > 0)
		output << mem.data[limit-1];
	output << "]";
	return output;
}
