#include <iostream>
#include <cstdlib>
#include <dlfcn.h>
#include "Log.h"
using namespace std;

int main()
{
  Log log;
  log.add("test lol\n");
  log.add("Retour à la ligne.\n");
  cout << log;
  log.rm();
  
  log.add("Serieux, serieux !!!");
  cout << log;
  log.rm();

  cout << "TEST AVEC >> " << endl;
  log << 1 << 2 << "\n" << " je veux afficher 1 et 2 ";
  cout << log;
  return 0;
}
