#include "authentification.h"

using namespace std;

SHA256_CTX context;
unsigned char pass_hash[SHA256_DIGEST_LENGTH];

string get_client_by_id(int id){
    ifstream fd(file_to_check, ios::in);
    string log_test;
    string pass_test;
    int cmp = 0;
    while( !fd.eof() && cmp != id){
	fd >> log_test >> hex >> pass_test;
	cmp++;
    }
    fd.close();
    return log_test;
}

int check_client(char *login, char *password){
    int i = 1, j;

    ifstream fd(file_to_check, ios::in);

    string log_test;
    string pass_test;
    char buffer[SHA256_DIGEST_LENGTH*7];
    char small_buff[SHA256_DIGEST_LENGTH*7];

    SHA256_Init(&context);
    SHA256_Update(&context, (unsigned char*)password, strlen(password));
    SHA256_Final(pass_hash, &context);

//    fprintf(stderr, "%d   tofind::\n", SHA256_DIGEST_LENGTH);

    sprintf(buffer, "%X", (unsigned int)pass_hash[0]);
    //   fprintf(stderr, "%X", (unsigned int)pass_hash[0]);
    int  compteur = strlen(buffer);
    for(j=1; j < (int)strlen((char *)pass_hash); j++){
//	fprintf(stderr, "%X", (unsigned int)pass_hash[j]);
	sprintf(small_buff, "%X",  (unsigned int)pass_hash[j]);
	compteur += strlen(small_buff);
	strcat(buffer, small_buff);
    }
    buffer[compteur] = '\0';
    //   fprintf(stderr, "\n-->%s\n", buffer);
    
    
    while( !fd.eof()){
	fd >> log_test >> pass_test;
//	cerr << log_test << " " <<  pass_test << endl;
	if(log_test.compare(login) == 0 && pass_test.compare(buffer) == 0){
	    fd.close();
	    return i;
	}
	i++;
    }
    //  cerr << "endl\n";
    return -1;
}

int add_client(char *login, char *password){
    string log_test;
    string pass_test;

    ifstream fd(file_to_check, ios::in );

    SHA256_Init(&context);
    SHA256_Update(&context, (unsigned char*)password, strlen(password));
    SHA256_Final(pass_hash, &context);
 

    while( !fd.eof()){
	fd >> log_test >> hex >> pass_test;
	if(log_test.compare(login) == 0){
	    fd.close();
	    return 0;
	}
    }
    fd.close();
    FILE* wd = fopen(file_to_check, "a");
    fprintf(wd, "%s ", login);
    for(int i = 0; i< (int ) strlen( (char *) pass_hash); i++){
	fprintf(wd, "%X", pass_hash[i]);
    }
    fprintf(wd, "\n");
    fclose(wd);
    return 1;
}
