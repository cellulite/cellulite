#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "interface_serveur.h"


int init_serveur(int port_, const char* adresse_)
{
	int ma_socket;
	struct sockaddr_in addr;
/*	sigset_t set;
	sigaddset(&set, SIGPIPE);
	sigprocmask(SIG_BLOCK, &set, NULL); //masque le signal qui peut être reçu avec read/write
*/	ma_socket = socket(PF_INET, SOCK_STREAM, 0);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port_);
//	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_addr.s_addr = inet_addr(adresse_);
	bind(ma_socket, (const struct sockaddr*) &addr, sizeof(struct sockaddr_in));
	listen(ma_socket, 15);

	return ma_socket;
}

char* so_filename(int owner, int family)
{
	//Retourne "code_[owner]_[family].so"
	//0 <= family < 10
	int length = 25;
	int ow = owner;
	while(ow > 0)
	{
		++length;
		ow /= 10;
	}
	char* filename = (char*) malloc(length * sizeof(char));
	sprintf(filename, "code_%d_%d.so", owner, family);
	return filename;
}
