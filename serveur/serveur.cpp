#include <iostream>
#include <queue>
#include <string>
#include <fstream>

#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>

#include "cClient.h"
#include "interface_serveur.h"
#include "reseau.h"
#include "authentification.h"
#include "environnement/env.h"
#include "environnement/Log.h"
#define FILE_COMP "codes/code__.cpp"
#define FILE_HIST "codes/compilation_gcc_report.txt"
#define FILE_G "codes/compil_prob_send"

using namespace std;

Log log_er;
fstream filestr;

int compt_fichier = 1;				// compteur pour injectivité des codes de cellules (un peu moche)
static unsigned int _env = 0;
Env environnement;

/* _nouvelles_cellules contient les noms des librairies dynamiques 
 * des cellules à ajouter.
 * entourée du mutex _mutex_cellules
 */
static queue < string > _nouvelles_cellules; 
/* _clients_connectes contient les clients "connectés".
 * entourée par _mutex_connectes
 */
static queue < Client* > _clients_connectes; 
/* _go est à true si les données sont prêtes à être envoyées.
 * entouré de _mutex_go
 */
static volatile bool _go;
/* compte le nombre de clients qui sont encore en cours de traitement
 * entouré de _mutex_occupes
 */
static volatile unsigned int _occupes;

static pthread_mutex_t 
_mutex_cellules, 
    _mutex_connectes, 
    _mutex_go,
    _mutex_occupes;

/* pour les cout << ...
 */
static pthread_mutex_t 	_mutex_test;

/* _cond_go signale aux threads émetteurs que les données sont prêtes.
 * _cond_stop signale au serveur que les données ont été envoyées.
 */
static pthread_cond_t _cond_go, _cond_stop,
    _cond_occupes;

static void _initialisation()
{
    //initialisation des mutex et des conditions
    _go = false;
    _occupes = 0;
    _clients_connectes.push(NULL);
    pthread_mutex_init(&_mutex_cellules, NULL);
    pthread_mutex_init(&_mutex_connectes, NULL);
    pthread_mutex_init(&_mutex_go, NULL);
    pthread_mutex_init(&_mutex_occupes, NULL);
    pthread_mutex_init(&_mutex_test, NULL);
    pthread_cond_init(&_cond_go, NULL);
    pthread_cond_init(&_cond_stop, NULL);
    pthread_cond_init(&_cond_occupes, NULL);
}

static void _finalisation()
{
    //destruction des mutex et des conditions
    pthread_mutex_destroy(&_mutex_cellules);
    pthread_mutex_destroy(&_mutex_connectes);
    pthread_mutex_destroy(&_mutex_go);
    pthread_mutex_destroy(&_mutex_occupes);
    pthread_mutex_destroy(&_mutex_test);
    pthread_cond_destroy(&_cond_go);
    pthread_cond_destroy(&_cond_stop);
    pthread_cond_destroy(&_cond_occupes);
}

/* Données chargées du .config :
 * _nb_emetteurs contient le nombre de threads émetteurs lancés.
 * _port_acceptation contient le numéro du port sur lequel les 
 * nouveaux clients se connectent pour recevoir la carte.
 * _port_reception contient le numéro du port sur lequel les codes
 * des nouvelles cellules sont reçues.
 * _delta_t est le nombre de tours que l'environnement fait entre
 * chaque émission de données
 */
static int 	_nb_emetteurs,
    _port_acceptation,
    _port_reception,
    _delta_t;

static string _adresse;

static char _entree_serveur[] = "entree_serveur";

static void _lit_config()
{
    //ouvre le .config et charge les paramètres du serveur.
    _nb_emetteurs = 20;
    _port_acceptation = 5546;
    _port_reception = 5547;
    _adresse = "140.77.166.19";
    _delta_t = 1;
}

void *thread_compilateur(void *arg) //arg sera un pointeur sur le nom du fichier
{
    /*provisoirement*/
    pthread_mutex_lock(&_mutex_test);
    {
	cout << "Compilation du fichier " + *((string*)arg) << "\n";
    }
    pthread_mutex_unlock(&_mutex_test);
    pthread_exit(EXIT_SUCCESS);
	//todo : system ...
}

/* un thread est chargé de détecter les connections des nouveaux clients
 * qui souhaitent recevoir la carte.
 */
void *thread_accepteur(void *arg) //arg vaudra _nb_threads + 1
{
  //   int rang = ((int *)arg)[0];
    int id = 0;
    int ma_socket, socket_nouveau_client;
    char buffer[MAX_LOGIN_SIZE];
    char login[MAX_LOGIN_SIZE];
    char password[MAX_PASS_SIZE];
    fd_set rdfs;
    struct sockaddr csin;
    socklen_t size = sizeof(csin);

    Client *nouveau_client;
    //initialise la socket de connection
    ma_socket = init_serveur(_port_acceptation, _adresse.c_str());
    while(1)
    {
	//attend une nouvelle connection
//	fprintf(stderr,"accepteur waiting there\n");
	fflush(stderr);
	FD_ZERO(&rdfs);
	FD_SET(ma_socket, &rdfs);
	select(FD_SETSIZE, &rdfs, NULL, NULL, NULL);
	if(FD_ISSET(ma_socket, &rdfs))
	{
	    //accepte la connection
	    socket_nouveau_client = accept(ma_socket, &csin, &size);
//	    fprintf(stderr, "accepteur commence a travailler!\n");
	    fflush(stderr);
	    read(socket_nouveau_client, buffer, 8);
	    buffer[7] = '\0';	    
	    read_word(socket_nouveau_client, login);
	    read_word(socket_nouveau_client, password);
	    if(!strcmp("inscrip", buffer)){ // C'est une inscription
			write(socket_nouveau_client, add_client(login, password) ? "registr" : "bad_log", 7);
			shutdown(socket_nouveau_client,2);
			continue;
	    }
	    id = check_client(login, password);
	    // fprintf(stderr, "%d %s %s\n", id, login, password);
	    if(strcmp("connect", buffer) || id < 1){
		write(socket_nouveau_client, "badauth", 7); 
		shutdown(socket_nouveau_client,2);
		continue;
	    }
	    sprintf(buffer, "%d", id);
	    fprintf(stderr, "\n%d\n", id);
	    write(socket_nouveau_client, buffer, strlen(buffer));
	    nouveau_client = new Client(id++, socket_nouveau_client);
	    //ajoute le nouveau client à _clients_connectes	
	    pthread_mutex_lock(&_mutex_connectes);
	    {
		_clients_connectes.push(nouveau_client);
	    }
	    pthread_mutex_unlock(&_mutex_connectes);
	}
	pthread_mutex_lock(&_mutex_test);
	{
	    cout << "ACCEPTEUR> nouveau client " << nouveau_client->id << "\n";
	}
	pthread_mutex_unlock(&_mutex_test);
	//boucle
    }
    pthread_exit(EXIT_SUCCESS);
}

void *thread_emetteur(void *arg) //arg sera compris entre 1 et _nb_threads (inclus)
{
    int rang = ((int *)arg)[0];
    bool fin_des_clients = false;
    Client *mon_client;
    while(1)
    {
	mon_client = NULL;
	//attend _go ou _cond_go (données prêtes)
//	fprintf(stderr,"emetteur waiting there\n");
	fflush(stderr);

	pthread_mutex_lock(&_mutex_go);
	{
	    while(!_go)
	    {
		pthread_cond_wait(&_cond_go, &_mutex_go);
	    }
	    pthread_mutex_lock(&_mutex_connectes);
	    {
		fin_des_clients = _clients_connectes.empty();
		if(!fin_des_clients)
		{
		    mon_client = _clients_connectes.front();
		    _clients_connectes.pop();
		    pthread_mutex_lock(&_mutex_occupes);
		    {
			++_occupes;
		    }
		    pthread_mutex_unlock(&_mutex_occupes);
		}
	    }
	    pthread_mutex_unlock(&_mutex_connectes);
	    if(!mon_client && !fin_des_clients)
	    {
		pthread_mutex_lock(&_mutex_occupes);
		{
		    if(--_occupes > 0)
		    {
			pthread_cond_wait(&_cond_occupes, &_mutex_occupes);
		    }
		    pthread_mutex_lock(&_mutex_connectes);
		    {
			_clients_connectes.push(mon_client);
		    }
		    pthread_mutex_unlock(&_mutex_connectes);
		    _go = false;
		    pthread_cond_signal(&_cond_stop);
		}
		pthread_mutex_unlock(&_mutex_occupes);
	    }
	}
	pthread_mutex_unlock(&_mutex_go);
//	fprintf(stderr, "emetteur waiting\n");
	fflush(stderr);
	
	if(mon_client)
	{
	    //envoie les données aux clients
	    if(envoie(_entree_serveur, mon_client->socket) == 0)
	    {
		pthread_mutex_lock(&_mutex_test);
		{
		    cout << "EMETTEUR> thread " << rang << " client " << mon_client->id << " env " << _env << "\n";
		}
		pthread_mutex_unlock(&_mutex_test);
		pthread_mutex_lock(&_mutex_connectes);
		{
		    _clients_connectes.push(mon_client);
		}
		pthread_mutex_unlock(&_mutex_connectes);
	    }
	    else //on déconnecte le client
	    {
		pthread_mutex_lock(&_mutex_test);
		{
		    cout << "EMETTEUR> thread " << rang << " client " << mon_client->id << " déconnecté\n";
		}
		pthread_mutex_unlock(&_mutex_test);
		shutdown(mon_client->socket, 2);
		delete mon_client;
	    }
	    pthread_mutex_lock(&_mutex_occupes);
	    {
		if(--_occupes == 0)
		{
		    pthread_cond_signal(&_cond_occupes);
		}
	    }
	    pthread_mutex_unlock(&_mutex_occupes);
	}
	//boucle
    }
    pthread_exit(EXIT_SUCCESS);
}

void *thread_recepteur(void *arg) //arg vaudra _nb_threads + 2
{
  //  int rang = (int)arg;
    int ma_socket, socket_client, num_famille;
    string code_cellule("code_cellule.so"), message_compilation("message_compilation"), error_message("error_message");
    fd_set rdfs;
    struct sockaddr csin;
    socklen_t size = sizeof(csin);
    char login[MAX_LOGIN_SIZE];
    char password[MAX_PASS_SIZE];
    char cell_size[MAX_PASS_SIZE];
    char mode[7];
    
    int fam = 0; // provisoire
    
    //pour le thread_compilateur
    //  pthread_t id_compilateur;
    pthread_attr_t attr_compilateur;
//    int status_compilateur = 0;
    pthread_attr_init(&attr_compilateur);
    pthread_attr_setdetachstate(&attr_compilateur, PTHREAD_CREATE_JOINABLE);

    //initialise la socket de réception
    ma_socket = init_serveur(_port_reception, _adresse.c_str());

    while(1)
    {
	//attend un code de cellule
//	fprintf(stderr, "recepteur waiting\n");
	fflush(stderr);
	FD_ZERO(&rdfs);
	FD_SET(ma_socket, &rdfs);
	select(FD_SETSIZE, &rdfs, NULL, NULL, NULL);
	if(FD_ISSET(ma_socket, &rdfs))
	{
//	    fprintf(stderr, "recepteur working\n");
	    fflush(stderr);
	    cerr << "waiting here" << endl;
	    //accepte la connection
	    socket_client = accept(ma_socket, &csin, &size);

//	    fprintf(stderr,"avant\n");
	    read(socket_client, mode, 7);
	    mode[6] = '\0';
	    read_word(socket_client, login);
	    //	    fprintf(stderr, "milieu\n");
	    read_word(socket_client, password);
	    //  fprintf(stderr, "après\n");			
	    //reçoit le fichier de code
//	    add_client(login, password);
//	    cerr << "par ici-" << mode << "-" << login << "-" << password << "-" << cell_size << "----" << endl;
	    int id_client = check_client(login, password); 
	    if(!strcmp(mode, "adding") && id_client>0)
	    {
		read_word(socket_client, cell_size);
		cerr << "entering before fam" << endl;
		fam = environnement.get_free_family(id_client);
		if( fam == -1)
		{
//			fprintf(stderr, "GUJtgj\n");
		    cerr << "fam vaut -1" << endl;
//		  	blabla
		    continue;
		}
		char* filename = so_filename(id_client, fam);
//		  if(recoit("codes/code__", socket_client) == 0)
		cerr << "avant reception " << atoi(cell_size) << endl;
		if(recoit("codes/code__", socket_client, atoi(cell_size)) == 0)
		{
		      
		      char* buff = (char*) malloc((100 + strlen(filename)) * sizeof(char));
		      sprintf(buff, "./graindecell codes/code__  > %s", FILE_COMP);
		      cerr << "avant premier appel" << endl;
		      int _t_e = system(buff);
		      if(!_t_e)
		      {
			  char* filename_2 = (char*) malloc((100+(int)(log(compt_fichier))+strlen(filename))*sizeof(char));
			  sprintf(filename_2,"codes/%d_%s", compt_fichier,filename);
			  compt_fichier ++;
			  sprintf(buff, "g++ -o %s -shared %s -fPIC 2> %s", filename_2, FILE_COMP, FILE_G);
			  cerr << "avant deuxieme appel" << endl;
			  if(system(buff) == 0){
			      pthread_mutex_lock(&_mutex_test);
			      {		
				  /* TODO	*/
				  //					fprintf(stderr, "Cellule en cours d'ajout\n");
				  cerr << "tout va bien" << endl;
				  environnement.add_cell_auto(filename_2, id_client);
				  // envoie(error_message.c_str(), socket_client);
//					fprintf(stderr, "Cellule ajoutée\n");
//				    send_compile_success(socket_client);
				  // on sauvegarde l'environnement dans save:
				  environnement.save_etat((char*)"save");
			      }
			      pthread_mutex_unlock(&_mutex_test);
			      sprintf(buff, "rm codes/code__.cpp"); // suppression du .cpp qui ne sert à rien
			      system(buff);
			      sprintf(buff, "rm codes/code__"); // suppression du code qui ne sert à rien
			      system(buff);
			  }
			  else
			  {
			      sprintf(buff, "cat %s >> %s", FILE_G, FILE_HIST);
			      system(buff);
			      fprintf(stderr, "Erreur : echec de la compilation (g++)\n");
//			  send_compile_failed(socket_client,(char*) FILE_G);
			      //Erreur
			  }
			  free(buff);
		      }
		      else
		      {
			  fprintf(stderr, "Erreur : echec de la compilation (graindecell)\n");
//		  send_compile_failed(socket_client,(char*) FILE_COMP);
			  //Erreur
			  
		      }
//		}
		      //envoie le message du compilateur au client
		      //	envoie(message_compilation.c_str(), socket_client);
		  }
	    }
	    if(!strcmp(mode, "delete") && id_client > 0){
		// récupérer la famille
		// delete client
//		cerr << "salut lsauté" << endl;
		num_famille = read_num_family(socket_client);
		cerr << "deleting mode..." << endl;
		environnement.rm_famille(id_client, num_famille-1);
		char* filename = so_filename(id_client, num_famille-1);
		char* buff = (char*) malloc((100 + strlen(filename)) * sizeof(char));
		sprintf(buff, "rm %s", filename); // suppression du .so qui ne sert à rien
		system(buff);
		fprintf(stderr, "suppression de la famille %d de %d\n", num_famille-1, id_client);
	    }
	}
	//boucle
    }
    pthread_attr_destroy(&attr_compilateur);
    pthread_exit(EXIT_SUCCESS);
}

int main(int argc, char** argv)
{
    _initialisation();
    _lit_config();
    pthread_t *thread_id = new pthread_t [_nb_emetteurs + 2];

    sigset_t set;
    sigaddset(&set, SIGPIPE);
    sigprocmask(SIG_BLOCK, &set, NULL); //masque le signal qui peut être reçu avec read/write

//    environnement.add_cell_auto("code_0_0.so", 1);
//    environnement.add_cell_auto("codes/code_compile2.so", 2);
//    environnement.add_cell_auto("codes/code_compile3.so", 3);
//    environnement.add_cell_auto("codes/code_compile4.so", 4);
//    environnement.add_cell_auto("codes/code_compile5.so", 5);

    //création des threads
    int *numero = new int [_nb_emetteurs];
    for(int i = 0 ; i < _nb_emetteurs ; ++i)
    {
		numero[i] = i;
		pthread_create(&thread_id[i], NULL, thread_emetteur, (void*)(numero + i));
    }
    pthread_create(&thread_id[_nb_emetteurs], NULL, thread_accepteur, NULL /*(void*)_nb_emetteurs*/);
    pthread_create(&thread_id[_nb_emetteurs + 1], NULL, thread_recepteur, NULL /*(void*)(_nb_emetteurs + 1)*/);

    // SI il y a un  ARGUMENT AU LANCEMENT ALORS load_etat(argument) ICI
    if (argc == 2)
      {
	environnement.load_etat((char*)"save");
      }
    
    while(1)
    {
      //ajoute une cellule si _nouvelles_cellules n'est pas vide
	/*
	  pthread_mutex_lock(&_mutex_cellules);
	  {
	  if(!_nouvelles_cellules.empty())
	  {
	  // TODO : ADD_CELL_HERE !
	  //environnement.add_cell(_nouvelles_cellules.front())
	  pthread_mutex_lock(&_mutex_test);
	  {
	  cout << "MAIN> nouvelle cellule : " << _nouvelles_cellules.front() << "\n";
	  }
	  pthread_mutex_unlock(&_mutex_test);
	  _nouvelles_cellules.pop();
	  }
	  }
	  pthread_mutex_unlock(&_mutex_cellules);
	*/
//exécute _delta t tours de l'environnement
	for(int i = 0 ; i < _delta_t ; ++i)
	{
	    //évolution de l'environnement
// TODO	    
	    usleep(200000);
	    pthread_mutex_lock(&_mutex_test);
	    {
		cout << "MAIN> exécution de l'environnement " << (_env++) << "\n";
		environnement.calc_tour();	
	    }
	    pthread_mutex_unlock(&_mutex_test);
	}
	//attend !_go ou _cond_stop (données envoyées)
	pthread_mutex_lock(&_mutex_go);
	{
	    if(_go) //si les threads n'ont pas fini
	    {
		pthread_cond_wait(&_cond_stop, &_mutex_go); //si on veut attendre
//				pthread_mutex_unlock(&_mutex_go);
//				continue;
	    }
	    //sauvegarde les données dans entree_serveur
	    //ouvre un ostream vers entree_serveur
	    /* TODO	   */ 
//	    fprintf(stderr, "on save\n");
	    fflush(stderr);
	    ofstream f(_entree_serveur);
//	    fprintf(stderr, "buisness open\n");
	    fflush(stderr);
	    if(!f.is_open()){
		fprintf(stderr, "Entree serveur closed\n");
		fflush(stderr);
		exit(EXIT_FAILURE);
	    }

//	    fprintf(stderr, "on save2\n");
	    fflush(stderr);
	    affiche_terrain(f, environnement);
//	    fprintf(stderr, "saved\n");
	    fflush(stderr);
	    f.close();
	    /**/
	    _go = true;
	    //signale _cond_go
	    pthread_cond_broadcast(&_cond_go);
	}
	pthread_mutex_unlock(&_mutex_go);
	//boucle
	
    }

	

    _finalisation();
    pthread_exit(EXIT_SUCCESS);
}
