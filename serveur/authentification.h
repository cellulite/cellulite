#ifndef AUTHEN
#define AUTHEN
#define file_to_check "registred_member"
#define MAX_LOGIN_SIZE 30
#define MAX_PASS_SIZE 30
#include <stdio.h>
#include <string.h>
#include <string>
#include <openssl/sha.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include "environnement/identifiant.h"

std::string get_client_by_id(int id);

int check_client(char *login, char *password);

int add_client(char *login, char *password);

int read_num_family(int socket);
#endif // AUTHEN
