//:reseau.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "reseau.h"
#include "authentification.h"

//taille du buffer utilisé pour la lecture et l'écriture
#define LONGUEUR 1000000
#define LONGUEUR_TEMP 10000
#define ACK "receive\n"
#define LONGUEUR_ACK 8

int envoie(const char* fichier_source, int socket)
{
    //fprintf(stderr, "entree dans envoie\n");
    // fflush(stderr);
    char Buffer_temp[LONGUEUR_TEMP];
    char Buffer_send[LONGUEUR];
    char *Buffer_F;
    Buffer_send[0] = '\0';
    int n_lu;
    int fd;
    struct timeval timeout; //quelle valeur mettre ??
    timeout.tv_sec = 0; //secondes
    timeout.tv_usec = 0; //microsecondes
    
    fd = open(fichier_source, O_RDONLY);
    if(fd < 0)
    {
	fprintf(stderr, "Erreur dans envoie : echec de l'ouverture de %s\n", fichier_source);
	return -1;
    }
    int cmp_max = 0;
    
    do
    {
	n_lu = read(fd, Buffer_temp, LONGUEUR_TEMP);
	Buffer_temp[n_lu] = '\0';
	if(n_lu < 0){
	    return -1;
	}
	cmp_max++;
	strcat(Buffer_send, (const char *)Buffer_temp);
    }while(n_lu == LONGUEUR_TEMP && cmp_max < 9);
    Buffer_F = new char [strlen(Buffer_send)+20];



    //fprintf(stderr, "laldaldaldal\n");
    // strcpy(buffer, "toto");
//    strcpy(buffer, buffer_send);
    sprintf(Buffer_F, "%d&\n%s", strlen(Buffer_send), Buffer_send); 
//    fprintf(stderr, "%d caract to sent:\n%s\n", strlen(Buffer_F), Buffer_F);
//    fflush(stderr);
    fd_set wrfs;
    FD_ZERO(&wrfs);
    FD_SET(socket, &wrfs);
    if(select(socket + 1, NULL, &wrfs, NULL, &timeout) <= 0)
	return -1;
    if(!FD_ISSET(socket, &wrfs))
	return -1;
    write(socket, Buffer_F, strlen(Buffer_F));
    close(fd);
    
    return 0; 
}

int recoit(const char* fichier_destination, int socket, int cell_size)
{
    char buffer[LONGUEUR];
//	char login[MAX_LOGIN_SIZE], password[MAX_PASS_SIZE];
    int n_rec, n_ecr;
    fd_set ens_rec;
    int sum_caract;
    int fd;
    

    struct timeval timeout; //quelle valeur ?
    timeout.tv_sec = 3;
    timeout.tv_usec = 0;
    fprintf(stderr, "youhou");
    fd = open(fichier_destination, O_WRONLY | O_CREAT | O_TRUNC, S_IREAD | S_IWRITE);
    if(fd < 0)
    {
	fprintf(stderr, "Erreur dans recoit : echec de l'ouverture de %s\n", fichier_destination);
	return -1;
    }
    fprintf(stderr, "before fd_zero\n");
    FD_ZERO(&ens_rec);
    FD_SET(socket, &ens_rec);
    if(select(FD_SETSIZE, &ens_rec, NULL, NULL, &timeout) <= 0)
    {
	fprintf(stderr, "Erreur dans recoit : timeout dépassé\n");
	return -1;
    }
    if(!FD_ISSET(socket, &ens_rec))
	return -1;
    
    fprintf(stderr, "before do while\n");
    sum_caract = 0;
    do
    {
	n_rec = read(socket, buffer, LONGUEUR);
	if(n_rec < 0){
	    return -1;
	}
	n_ecr = write(fd, buffer, n_rec);
	if(n_ecr != n_rec)
	    return -1;
	sum_caract += n_rec;
    }while(sum_caract < cell_size);
   
//	n_ecr = write(socket, ACK, LONGUEUR_ACK);
    fprintf(stderr, "recu %d/%d caracteres\n", sum_caract, cell_size);
    close(fd);
    return 0;
}



int read_word(int sock, char result[]){
    int cmp = 0;
    char temp[2];
    do{
	if(read(sock, temp, 1)<0) return -1;
	result[cmp++] = temp[0];
    }while(temp[0] != ' ' && temp[0] != '\n' && cmp < MAX_PASS_SIZE);
    
    result[cmp-1] = '\0';
    //    fprintf(stderr, "%s\n", result);
    
    return cmp;
}


int read_num_family(int sock){
    int res;
    FILE *stream = fdopen(sock, "r+");
    fscanf(stream, "%d\n", &res);
    fclose(stream);
    return res;
}


void send_compile_failed(int socket, char *file_to_send){
    int fd = open(file_to_send, O_WRONLY | O_CREAT | O_TRUNC, S_IREAD | S_IWRITE);
    int n_rec, n_ecr;
    char buffer[LONGUEUR] = "echec\n";
    n_ecr = write(socket, buffer, 6);
   
    do
    {
	n_rec = read(fd, buffer, LONGUEUR);
	if(n_rec < 0){
	    return ;
	}

	n_ecr = write(socket, buffer, n_rec);
	fprintf(stderr, "%s\n --> %d %d\n", buffer, n_rec, n_ecr);
	fflush(stderr);
	if(n_ecr != n_rec)
	    return ;
    }
    while(n_rec == LONGUEUR);
    close(fd);
    close(socket);
    fprintf(stderr, "fin de l'envoi");
}

void send_compile_success(int socket){
    char buffer[LONGUEUR];
    sprintf(buffer, "COMPILE SUCCESSFULL\n");
    write(socket, buffer, strlen(buffer));
    close(socket);
}
