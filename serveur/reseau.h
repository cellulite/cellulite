//:reseau.h
/***************************************************************************\
 *Fonctions très utiles pour le réseau, que se soit le serveur ou le client.
\***************************************************************************/

#ifndef DEF_RESEAU
#define DEF_RESEAU
#define MAX_SIZE_RECEP 16000


//envoie sur la socket le contenu du fichier fichier_source
//un réponse est attendue de la part du récepteur pour déceler la fin
//empêcher les autres threads de commencer à parler avant la fin de la
//réception
int envoie(const char* fichier_source, int socket);

//reception sur la socket et écriture des données reçues dans le fichier
//fichier_destination
//envoie une réponse à l'émetteur une fois le fichier reçu totalement
int recoit(const char* fichier_destination, int socket, int cell_size);


int read_word(int sock, char *result);

int read_num_family(int sock);

void send_compile_failed(int socket, char *file_to_send);
void send_compile_success(int socket);

#endif //guard
