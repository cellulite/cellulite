open Ast

type assignement = Assign of expr * expr 
	(* la première expr doit correspondre à une variable *)

(*
and
	action = 
	| Move of expr 
	(* on se déplace dans la première direction de l'expr *)
	(* pas de déplacement si aucune direction *)
	| Send_M of expr * expr
	| Send_E of expr * expr
	| Duplicate of expr * expr * string * expr list * expr list * int 
	(* nrj, direction, nom de mon_action, vars, args, taille du code *) 
	| Call of string * expr list * expr list * assignement list * int
	(* nom_prog, adresses (absolues ou relatives) des args, args, assignation initiale, debut relatif comme convenu *)
	| Skip
*)
and
  addr =
  | Abs of int (* adresse absolue dans la mémoire *)
  | Rel of int (* adresse relative dans la mémoire (+ debut)*)
  | Arg of int (* numéro de l'argument *)
  | PVar of int (* numéro de la variable  *)

and
  message = expr
and
  expr = 
  |Var of string 
	(* variable issue d'un let ... in*)
  |Refvar of addr 
	(* adresse relative ou absolue dans la mémoire *)
  | Int of int
  | Const of string 
	(* Constantes : âge, énergie ... ==> cell->age*)
  | Op_expr of op*expr*expr
  | Function of string * expr list (* fonctions d'énergie, etc... *)
  (*  |Dir_int of int
      |Dir_vect of expr list (* à reconsidérer *)*)
  | DIR_MESS of string * expr
  | SEE
  | Sub_expr of message * expr * expr (* Message[i:j] : attention aux segfault *)
  | Concat of (expr*int) list (* Concat([(m1,t1);(m2,t2)]) est le message formé des t1 premiers bits de m1 suivi des t2 premiers bits de m2 *)
  | Not of expr
      
and
  action = 
  |Move of expr
  |Send_M of message * expr
  |Send_E of expr * expr (* énergie, direction *)
  |Duplicate of expr * expr * string * expr list * expr list * int (*taille du code*) 
	(* modification due à l'ajout d'arguments distincts des variables *)
  |Call of string * addr list * expr list * assignement list * int 
	(* nom du prog, vars, args, initialisation, debut *)
  |Skip
and
  prog = 
  | IFTE of expr * prog * prog
  | IFTE_M of string * string * expr *prog * prog (* message, direction, condition, then,else *)
  | LETIN of string * expr * prog (* remarque : que se passe-t-il s'il y a deux let in imbriqués avec le même nom de variable? C'est autorisé par Caml... Mais sans renommage, ça risque d'être impossible *)
  | Action_c of assignement * prog
  | Action of action
  | Progseq of string * prog (* correspond à "label: programme" *)
and
  decl_prog = 
    DecProg of string * (addr * string) list * prog (* la liste correspond aux tests de début des sequences *)
and 
  adn = 
    ADN of decl_prog list * string * expr list * expr list (* ce serait bien qu'elle soit dans l'ordre, Thomas le suppose dans traduction.ml [write_mem_init] *) * int (* taille du main *)
