principale : vars (dejaDup, dir) args()
  if (dejaDup = 0) then
    if (energy > 200) then
      (
	dejaDup := 1;
	Duplicate((energy / 2) bor rand(), dir, principale(0, dir)())
      )
    else
      (
	if (not(See() bor NextDir(N))) then
	  (Skip)
	else
	  Skip
      )
  else
    Skip
  ;;
@principale(0, N)().
