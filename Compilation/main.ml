open Parser
open Lexer
open Ast
open Pre_traitement
open Ast2
open Error
open Traduction
open Anastat
open Ast_to_ast2

let soi = string_of_int
let p = print_string

let argc = Array.length Sys.argv ;;
let main() = 
  if (argc <> 2) then 
    (p ("use syntax :  ./graindecell [source_code_file_name] "));
  
  let filename = Sys.argv.(1) in    
  let oc = open_in filename in
  let lexbuf = Lexing.from_channel oc in
  
  let parse() = try (Parser.main Lexer.token lexbuf) with
    | Parsing.Parse_error -> raise (Error.Parse_error(!Lexer.ligne,!Lexer.caractere))  in
  let x = parse() in
  let pre_traite () = Pre_traitement.seq_adn x in
  let y = pre_traite () in
  Anastat.init y;
  
  let z = Ast_to_ast2.trad_adn y in
(*          Ast_to_ast2.p_adn z;*)
  
  print_string (Traduction.affiche (Traduction.transform z));;

Error.testErreurs main

    
