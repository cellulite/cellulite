{
  open Ast
  open Parser;;        (* le type "token" est d�fini dans parser.mli *)
  exception Eof;;
  let caractere = ref 1;;
  let ligne = ref 1;;
  let soi = string_of_int
  let is_comment = ref 0 (* on va permettre les (*(*... *) *) *)
  let traite tok token lexbuf = if (!is_comment = 0) then tok else token lexbuf 
}

let digit0 = '0' | '1'
let word = digit0+
let digit = ['0'-'9']
let number = digit+
let letter = ['a'-'z'] | ['A'-'Z'] | '_'
let sletter = ['a'-'z'] | ['A'-'Z']
let ident = sletter (letter | digit)*
let string = '\'' [^'\'']* '\''


(* let commentaire = '{' [^'}']* '}' *)

rule token = parse
  | [' ' '\t']     { incr caractere; token lexbuf }

(* A v�rifier comment  | commentaire     { token lexbuf } *)

  | '\n'            { caractere := 1;incr ligne; token lexbuf}
(*  | word as k       { caractere := !caractere + String.length k; WORD k }*)
  | number as k     { caractere := !caractere + String.length k; traite (INT (int_of_string k)) token lexbuf }
 (* | ":="            { caractere := !caractere + 2; ASSIGN }*)
  | '='             { incr caractere; traite EQ token lexbuf }
  | "<="            { caractere := !caractere + 2; traite LE token lexbuf }
  | ">="            { caractere := !caractere + 2; traite GE token lexbuf }
  | '<'             { incr caractere; traite LT token lexbuf }
  | '>'             { incr caractere; traite GT token lexbuf }
  | "<>"            { caractere := !caractere + 2; traite NEQ token lexbuf }
  | "%"             { incr caractere; traite MOD token lexbuf }


  | '+'             { incr caractere; traite PLUS token lexbuf }
  | '-'             { incr caractere; traite MINUS token lexbuf }
  | '*'             { incr caractere; traite TIMES token lexbuf }
  | '/'             { incr caractere; traite DIV token lexbuf }

  | '@'             { incr(caractere); traite AROBASE token lexbuf }
 (* | '#'             { incr(caractere); traite SHARP token lexbuf }*)

  | '&'             { incr(caractere) ; traite REF token lexbuf }

  | "vars"          { caractere := !caractere + 4 ; traite VARS token lexbuf }
  | "args"          { caractere := !caractere + 4 ; traite ARGS token lexbuf }

  | '('             { incr caractere; traite LPAREN token lexbuf }
  | ')'             { incr caractere; traite RPAREN token lexbuf }
  | '['             { incr caractere; traite LBRACKET token lexbuf }
  | ']'             { incr caractere; traite RBRACKET token lexbuf }
  | '{'             { incr caractere; traite LBRACE token lexbuf }
  | '}'             { incr caractere; traite RBRACE token lexbuf }
  | "(*"            { incr(is_comment) ; caractere := !caractere + 2 ; token lexbuf }
  | "*)"            { decr(is_comment) ; caractere := !caractere + 2 ; token lexbuf }
  | ','             { incr caractere; traite COMMA token lexbuf }
  | ';'             { incr caractere; traite SEMICOLON token lexbuf }
  | '.'             { incr caractere; traite DOT token lexbuf }
  | ':'             { incr caractere; traite COLON token lexbuf }


  | "and"           { caractere := !caractere + 3; traite AND token lexbuf }
  | "or"            { caractere := !caractere + 2; traite OR token lexbuf }
  | "not"           { caractere := !caractere + 3; traite NOT token lexbuf }
  | "bor"           { caractere := !caractere + 3; traite BOR token lexbuf}
  | "band"          { caractere := !caractere + 4; traite BAND token lexbuf}

  | "if"            { caractere := !caractere + 2; traite IF token lexbuf }
  | "then"          { caractere := !caractere + 4; traite THEN token lexbuf }
  | "else"          { caractere := !caractere + 4; traite ELSE token lexbuf }
  | "begin"          { caractere := !caractere + 5; traite BEGIN token lexbuf }
  | "end"          { caractere := !caractere + 3; traite END token lexbuf } 

  | "let"           { caractere := !caractere + 3; traite LET token lexbuf }
  | "in"            { caractere := !caractere + 2; traite IN token lexbuf }

  | "N"              { caractere := !caractere + 1; traite (INT 1) token lexbuf }
  | "NE"             { caractere := !caractere + 2 ; traite (INT 2) token lexbuf}
  | "E"              { caractere := !caractere + 1 ; incr(caractere) ; traite (INT 4) token lexbuf}
  | "SE"             { caractere := !caractere + 2; traite (INT 8) token lexbuf}
  | "S"              { caractere := !caractere + 1 ; traite (INT 16) token lexbuf}
  | "SO"             { caractere := !caractere + 1 ; traite (INT 32) token lexbuf}
  | "O"              { caractere := !caractere + 1 ; traite (INT 64) token lexbuf }
  | "NO"             { caractere := !caractere + 2;  traite (INT 128) token lexbuf}

  | "See"           { caractere := !caractere + 3; traite SEE token lexbuf }
  | "DirMessage"   { caractere := !caractere + 11; traite DIR_MESSAGE token lexbuf }
  | "Exists"        { caractere := !caractere + 6; traite EXISTS token lexbuf}
  | "Concat"        { caractere := !caractere + 6; traite CONC token lexbuf}


 (* | "Message"       { caractere := !caractere + 7; traite MESSAGE token lexbuf } *) (* je r�ve ou bien cette ligne ne sert � rien? *)
  | "Move"          { caractere := !caractere + 4; traite MOVE token lexbuf }
  | "SendMessage"  { caractere := !caractere + 12; traite SEND_MESSAGE token lexbuf   }
  | "SendEnergy"   { caractere := !caractere + 11; traite SEND_ENERGY token lexbuf }
  | "Duplicate"     { caractere := !caractere + 8; traite DUPLICATE token lexbuf }
  | "&&"            {caractere := !caractere + 2; traite SUCC token lexbuf}
  | "Skip"          { caractere := !caractere + 4; traite SKIP token lexbuf }



  | ident as k      { caractere := !caractere + String.length k; traite (IDENT k) token lexbuf }
  | eof             { raise Eof }
  | _          { if (!is_comment <> 0) then token lexbuf else raise(Ast.Generic_Error("Syntax Error : at line "^( soi(!ligne))^", caracter "^(soi(!caractere))))}
