(* Contient des fonctions de type Ast.adn -> Ast.adn qui cherche     *)
(* des optimisations avant tout le reste (déroulement des séquences, *)
(* propagation des constantes)                                       *)

open Ast

let rec seq_adn = function
	| Ast.ADN(dec_p_l, name, vars, args) -> 
		Ast.ADN(List.map seq_dec_prog dec_p_l, name, vars, args)

and seq_dec_prog = function
	| Ast.DecProg(name, vars, args, p) -> 
		Ast.DecProg(name, vars, args, seq_prog p)

and seq_prog = function
	| Ast.IFTE(e, p1, p2) -> Ast.IFTE(e, seq_prog p1, seq_prog p2)
	| Ast.IFTE_M(var_mess, var_dir, cond, p1, p2) ->
		Ast.IFTE_M(var_mess, var_dir, cond, seq_prog p1, seq_prog p2)
	| Ast.LETIN(var, e, p) -> Ast.LETIN(var, e, seq_prog p)
	| Ast.Action_c(assign, p) -> Ast.Action_c(assign, seq_prog p)
	| Ast.Action(a) -> Ast.Action(a)
	| Ast.Seq(p_l) -> begin
		let p_l2 = List.map seq_prog p_l in
		let aux p l = match p with
			| Ast.Seq(l2) -> List.concat [l2; l]
			| p2 -> p2 :: l in
		Ast.Seq(List.fold_right aux p_l2 [])
		end
;;

(* Ne pas regarder la suite, ce n'est pas fini et ce qui est *)
(* fait n'est même pas correct !!!                           *)
(*
let taille_hash = 251
and coeff_hash = 13
;;

module Var =
	struct
		type t = string
		let equal s1 s2 = ((String.compare s1 s2) = 0)
		let hash s = 
			let res = ref 0 in
			for i = 0 to (String.length s - 1) do
				res := (!res + coeff_hash * (int_of_char s.[i])) mod taille_hash;
			done;
			!res
	end

module IntHtbl = Hashtbl.Make(Var)

let tab_i = IntHtbl.create taille_hash
;;
let add_i = IntHtbl.add tab_i
and rem_i = IntHtbl.remove tab_i
;;

let rec prop_adn = function
	| Ast.ADN(dec_p_l, name, vars, args) -> 
		let vars2 = List.map prop_expr vars
		and args2 = List.map prop_expr args in
		Ast.ADN(List.map prop_dec_prog dec_p_l, name, vars2, args2)

and prop_dec_prog = function
	| Ast.DecProg(name, vars, args, p) -> 
		Ast.DecProg(name, vars, args, prop_prog p)

and prop_prog = function
	| Ast.IFTE(e, p1, p2) -> let e1 = prop_expr e in
		Ast.IFTE(e1, prop_prog p1, prop_prog p2)
	| Ast.IFTE_M(var_mess, var_dir, cond, p1, p2) -> 
		let cond1 = prop_expr cond in
		Ast.IFTE_M(var_mess, var_dir, cond1, prop_prog p1, prop_prog p2)
	| Ast.LETIN(var, e, p) -> begin
		let e1 = prop_expr e in
		match e1 with
			| Ast.Int(n) -> add_i var e1;
				let p1 = prop_prog p in
				rem_i var;
				Ast.LETIN(var, e1, p1)
(*			| Ast.Message(Mess(mess)) -> add_m var mess
				let p1 = prop_prog p in
				rem_m var;
				Ast.LETIN(var, e1, p1)
*)			| _ -> Ast.LETIN(var, e1, prop_prog p)
		end
	| Ast.Action_c(Ast.Assign(var, e), p) -> begin
		let e1 = prop_expr e in
		match e1 with
			| Ast.Int(n) -> add_i var e1;
				let p1 = prop_prog p in
				rem_i var; (* Aie aie aie *)
				Ast.Action_c(var, e1, p1)
(*			| Ast.Message(Mess(mess)) -> add_m var mess
				let p1 = prop_prog p in
				rem_m var; (* Aie aie aie *)
				Ast.LETIN(var, e1, p1)
*)			| _ -> (* Aie aie aie *)
				Ast.Action_c(Ast.Assign(var, e1), prop_prog p)
		end
	| Ast.Action(a) -> Ast.Action(prop_action a)
	| Ast.Seq(p_l) -> Ast.Seq(List.map prop_prog p_l)
		(* Que faire ?? Il ne faut pas supprimer les vars avant la fin de la séquence *)

and prop_action = function
	| Ast.Move(e) -> Ast.Move(prop_expr e)
	| Ast.Send_M(mess, e) -> Ast.Send_M(mess, prop_expr e)
	| Ast.Send_E(nrj, dir) -> Ast.Send_E(prop_expr nrj, prop_expr dir)
	| Ast.Duplicate(nrj, dir, name, vars, args) -> 
		Ast.Duplicate(prop_expr nrj, prop_expr dir, name, List.map prop_expr vars, List.map prop_expr args)
	| Ast.Call(name, e_a_l, e_l) -> 
		Ast.Call(name, List.map prop_expr_addr e_a_l, List.map prop_expr e_l)
	| Skip -> Skip

and prop_expr_addr = function
	| Exp(e) -> Exp(prop_expr e)
	| Addr(e) -> (* Aie aie aie *)
*)
