#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config_comm.h" //Contient le port et l'adresse du serveur
#include "interface_client.h"
/*
#if defined() //Windows
	#include <winsock2.h>
	#include <io.h>
#elif defined(linux) //Linux
	#include <sys/socket.h>
	#include <unistd.h>
	#include <fcntl.h>
	typedef int SOCKET;
	typedef struct sockaddr_in SOCKADDR_IN;
	typedef struct sockaddr SOCKADDR;
	typedef struct in_addr IN_ADDR;
#elif define() //Mac
#else //Autre
#endif
*/

#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

static SOCKET Ma_socket;
static SOCKADDR_IN Serveur;
static char Buffer[TAILLE_BUFF];
static FILE *stream;

/*
int init()
{
#if defined() //Windows
	WSADATA wsa;
	int err = WSAStartup(MAKEWORD(2, 2), &wsa);
	return (err < 0);
#else
	return 0;
#endif
}

int term()
{
#if defined() //Windows
	return WSACleanup();
#else
	return 0;
#endif
}
*/

int connecte_serveur()
{
//	struct hostent* info_serveur;
	struct hostent* info_serveur = gethostbyname("localhost");
//	if(!info_serveur)
//		printf("Failure\n");

	if((Ma_socket = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		return -1;

/*	info_serveur = gethostbyname(ADRESSE_SERVEUR);
	if(info_serveur == NULL)
	{
		fprintf(stderr, "Erreur dans connecte_serveur : serveur %s non trouvé.\n", ADRESSE_SERVEUR);
		return -1;
	}
*/	
	Serveur.sin_family = AF_INET;
	Serveur.sin_port = htons(PORT_SERVEUR);
	Serveur.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
//	Serveur.sin_addr.s_addr = htonl(INADDR_ANY);
//	Serveur.sin_addr = *(IN_ADDR *) info_serveur->h_addr;
//	printf("Ma socket : %d\n", Ma_socket);
	if(connect(Ma_socket, (SOCKADDR*) &Serveur, sizeof(SOCKADDR)))
		return -1;
	if(write(Ma_socket, (void*)"Nouvelle connexion", 18) < 0)
		return -1;
	stream = fdopen(Ma_socket, "r");
	return 0;
}

int recoit_carte(/*structure d'une carte, */)
{
    int n_rec = 0, reception = 0, i, nb_lines;
	fd_set ens_rec;
	FD_ZERO(&ens_rec);
	FD_SET(Ma_socket, &ens_rec);
	if(select(FD_SETSIZE, &ens_rec, NULL, NULL, NULL) < 0)
	{
		return -1;
	}
	fprintf(stdout, "CLIENT> réception :\n");
	if(FD_ISSET(Ma_socket, &ens_rec))
	{
	    fscanf(stream, "%d \n", &nb_lines);
	    fprintf(stderr, "nb_lines : %d\n", nb_lines);
	    i = 0;
	    do
	    {
		fgets(Buffer, 10, stream);
		fprintf(stderr, " J'archive : %s ", Buffer);
		if(Buffer[strlen(Buffer)-1] == '\n') i++;
	    }
	    while(i < nb_lines);
	}
	return 0;
}

/*
int envoie_cellule(const char* fichier, int mon_id)
{
	int descripteur;
	int n_lu = 0, n_env = 0;
#if defined() //Windows
	descripteur = _open(fichier, _O_BINARY | _O_RDONLY);
#elif defined() //Linux
	descripteur = open(fichier, O_RDONLY);
#elif defined() //Mac
#endif
	if(descripteur == -1)
		return -1;
	sprintf(buffer, "%d cellule\n", mon_id);
	do
	{
		n_lu = read(descripteur, (void*) Buffer, TAILLE_BUFF);
		n_env = write(Ma_socket, (void*) Buffer, n);
		if(n_env < n_lu)
		{
			close(descripteur);
			return -1;
		}
			
	} while(n_lu == TAILLE_BUFF);
}
*/

int deconnecte_serveur()
{
	printf("CLIENT> déconnection\n");
	fclose(stream);
	return shutdown(Ma_socket, 2);
}
