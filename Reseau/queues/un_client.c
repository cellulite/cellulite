#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "interface_client.h"

int main()
{
	int i;
	if(connecte_serveur())
	{
		fprintf(stderr, "Veuillez nous excuser pour l'indisponibilité du serveur. Vous pouvez vous reporter à notre site internet pour plus d'informations.\n");
		return 1;
	}
	for(i = 0 ; i < 20 ; ++i)
	{
		if(recoit_carte() == -1)
		{
			fprintf(stderr, "Serveur interrompu.\n");
			return 0;
		}
	}
	deconnecte_serveur();
	return 0;
}
