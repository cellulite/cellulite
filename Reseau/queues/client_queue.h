#ifndef DEF_LISTES
#define DEF_LISTES

#include "client.h"

void init_client_queues();
client pop_client();
client pop_nouveau();
void push_client(client c);
void push_nouveau(client c);

// pour les tests
void print();

#endif //guard
