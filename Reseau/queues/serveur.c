#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>

#include "config_comm.h"
#include "config_serveur.h"
#include "client.h"
#include "client_queue.h"
int i = 0;


int transfertFichier(char buffer[], char* fichier, FILE* destination){
	FILE* source;
	source = fopen(fichier, "r");
	if(!source)
		return 0;
	if(!destination){
		return 0;
	}
//	fprintf(destination, "Debut\n");
	while(fgets(buffer, TAILLE_BUFF, source)){
		fprintf(destination, "%s", buffer);
		fflush(destination);
	}
//	fprintf(destination, "Fin\n");
	fclose(source);
	return 1;
}

void *Threads_waiting(void *arg)
{
	char buffer[500];
	int my_id = (int)arg;
	int erreur = 0;
	client mon_client;
	fprintf(stdout, "thread %d ready\n", my_id);
	
	while(1)
	{

		if(mon_client = pop_nouveau())
		{
//			TRAITEMENT
			fprintf(stderr, "T:%d --> decouvre client %d\n", my_id, mon_client->id);
			push_client(mon_client);
		}
		if(mon_client = pop_client())
		{
//			TRAITEMENT
		    sleep(1);
			fprintf(stderr, "T:%d --> traite client %d\n", my_id, mon_client->id);
			char* message = "5\n1 23 42 10 15\n2 32 24 20 20\n12 12 12 50 50\n1 23 41 9 16\n1 24 44 11 14\n";
			char* message2 = "5\n1 23 10 10 15\n2 32 24 15 15\n12 12 12 11 50\n1 23 41 9 16\n1 24 44 50 14\n";
			if(i%2)
			    sprintf(buffer, message);
			else
			    sprintf(buffer, message2);
			i = (i + 1)%2;
			erreur = (send(mon_client->socket, (void*)buffer, strlen(buffer), MSG_NOSIGNAL) < 0);
			erreur = (write(mon_client->socket, (void*)buffer, strlen(buffer)) < 0);
			if(!erreur)
				push_client(mon_client);
			else
			{
				close(mon_client->socket);
				free(mon_client);
				printf("T:%d --> client déconnecté\n", my_id);
			}
		}
//		sleep(1);
	}
//	printf("T:%d --> exit\n", my_id);
	pthread_exit(EXIT_SUCCESS);
}



int main()
{  
/*
	struct timespec timeout;
	timeout.tv_sec = 2;
	timeout.tv_nsec = 0;
*/
/*
//Initialisation du serveur
	int my_id = NB_THREADS;
	struct sockaddr_in addr;
	int listen_fd;
	int one = 1;
	int client_id;
	size_t length;
	struct sockaddr_in* client_addr = NULL;
	int i;
	pthread_attr_t attr;

	listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, & one, sizeof(int));
	memset(& addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(1234);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	fprintf(stdout, "Adresse : %u\n", addr.sin_addr.s_addr);
	bind(listen_fd, (const struct sockaddr*) &addr, sizeof(struct sockaddr_in));
	listen(listen_fd, 15);

	init_client_queues();

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for(i = 0; i<NB_THREADS; i++)
	{
		pthread_create(&id[i], &attr, Threads_waiting, (void*)i);
	}
	i = 0;
	while(1)
	{
		client_addr = malloc(sizeof(struct sockaddr_in));
		client_id = accept(listen_fd, (struct sockaddr*) client_addr, &length);
		push_nouveau(new_client(client_id, client_addr, length));
	}
*/

	int my_id = NB_THREADS, nouveau_client;
	pthread_t id[NB_THREADS];
	fd_set rdfs;
	client c;
	struct sockaddr_in addr;
	struct sockaddr csin;
	socklen_t size = sizeof(csin);
	int my_sock = socket(PF_INET, SOCK_STREAM, 0);
	int i;
	char buffer[50];
	pthread_attr_t attr;
	sigset_t set;
	sigaddset(&set, SIGPIPE);
	sigprocmask(SIG_BLOCK, &set, NULL);
//	printf("Ma socket de serveur : %d\n", my_sock);
	

	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT_SERVEUR);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
//	addr.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
	bind(my_sock, (const struct sockaddr*) &addr, sizeof(struct sockaddr_in));
	listen(my_sock, 15);

	init_client_queues();

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for(i = 0; i<NB_THREADS; i++)
	{
		id[i] = i;
		pthread_create(&id[i], &attr, Threads_waiting, (void*)i);
	}
/**/
	while(1)
	{
		FD_ZERO(&rdfs);
		FD_SET(my_sock, &rdfs);
		select(FD_SETSIZE, &rdfs, NULL, NULL, NULL);
		printf("Un client\n");
		if(FD_ISSET(my_sock, &rdfs))
		{
			nouveau_client = accept(my_sock, &csin, &size);
			i = read(nouveau_client, (void*)buffer, 49);
			buffer[i] = '\0';
			printf("SERVEUR> reçu : %s\n", buffer);
//			send(nouveau_client, (void*)"Coucou, je suis le serveur\n", 27, 0);
			c = new_client(nouveau_client);
			push_nouveau(c);
//			print();
		}
	}
/**/
/** /
	i = 0;
	while (i < 5)
	{
		c = new_client(i++);
		push_nouveau(c);
		print();
		sleep(5);
	}
	while(1) sleep(20);
/ **/	return 0;
}
