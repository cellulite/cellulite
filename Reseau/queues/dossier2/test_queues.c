#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "config_serveur.h"
#include "client_queue.h"
#include "client.h"

void *execution_thread(void *arg)
{
	int my_id = (int)arg;
	client mon_client;
	fprintf(stderr, "T:%d --> prêt\n", my_id);
	while(1)
	{

		mon_client = pop_nouveau();
		if(mon_client != NULL)
		{
			fprintf(stderr, "T:%d --> decouvre client %d\n", my_id, mon_client->id);
			push_client(mon_client);
		}
		mon_client = pop_client();
		if(mon_client != NULL)
		{
			fprintf(stderr, "T:%d --> traite client %d\n", my_id, mon_client->id);
			push_client(mon_client);
		}
//		sleep(1);
	}
	pthread_exit(EXIT_SUCCESS);
}

int main()
{
	int i, socket_client;
	pthread_t thread_id[NB_THREADS];
	pthread_attr_t attr;
	client c;

	init_client_queues();
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for(i = 0; i<NB_THREADS; i++)
	{
		pthread_create(&thread_id[i], &attr, execution_thread, (void*)i);
	}
	for(i = 0 ; i < 100 ; ++i)
	{
		socket_client = 2 * i;
		c = new_client(socket_client);
		push_nouveau(c);
//		sleep(5);
	}
	while(1)
		sleep(60);
	return 0;
}
