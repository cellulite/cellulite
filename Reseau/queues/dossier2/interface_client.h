//:interface_client.h
/***************************************************************************\
 * Fonctions utiles pour l'implémentation d'un client et du serveur.
 * Comme pour les files, le plus de chose possible est dissimulé dans le .c
\***************************************************************************/

#ifndef DEF_INTERFACE_CLIENT
#define DEF_INTERFACE_CLIENT

//Correct return value is 0 !!!

//int init(); //pour Windows
//int term(); //pour Windows

int connecte_serveur();
int recoit_carte(char* fichier_destination);
int envoie_cellule(char* fichier_source);
int deconnecte_serveur();

#endif //guard
