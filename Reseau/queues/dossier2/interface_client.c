//:inteface_client.c
/***************************************************************************\
 * Tout est dans réseau.h sauf connection et déconnection
 * Les typedef sont juste là pour décorer mais si on souhaite adapter le 
 * code pour Windows, cela semble pratique ^^
\***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "config_comm.h" //Contient le port et l'adresse du serveur
#include "interface_client.h"
/*
#if defined() //Windows
	#include <winsock2.h>
	#include <io.h>
#elif defined(linux) //Linux
	#include <sys/socket.h>
	#include <unistd.h>
	#include <fcntl.h>
	typedef int SOCKET;
	typedef struct sockaddr_in SOCKADDR_IN;
	typedef struct sockaddr SOCKADDR;
	typedef struct in_addr IN_ADDR;
#elif define() //Mac
#else //Autre
#endif
*/

#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "reseau.h"
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

static SOCKET Ma_socket;
static SOCKADDR_IN Serveur;

/*
int init()
{
#if defined() //Windows
	WSADATA wsa;
	int err = WSAStartup(MAKEWORD(2, 2), &wsa);
	return (err < 0);
#else
	return 0;
#endif
}

int term()
{
#if defined() //Windows
	return WSACleanup();
#else
	return 0;
#endif
}
*/

int connecte_serveur()
{
	if((Ma_socket = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		return -1;

	Serveur.sin_family = AF_INET;
	Serveur.sin_port = htons(PORT_SERVEUR_1);
//	Serveur.sin_addr.s_addr = htonl(INADDR_ANY);
	Serveur.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
//	struct hostent* info_serveur;
//	info_serveur = gethostbyname("localhost");
//	if(!info_serveur)
//		return -1;
//	Serveur.sin_addr = *(IN_ADDR *) info_serveur->h_addr;
	if(connect(Ma_socket, (SOCKADDR*) &Serveur, sizeof(SOCKADDR)))
		return -1;
	return 0;
}

int recoit_carte(char* fichier_destination)
{
	return recoit(fichier_destination, Ma_socket);
}

int envoie_cellule(char* fichier_source)
{
	SOCKET socket;
	SOCKADDR_IN addr_serveur;
	int err;
	if((socket = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		return -1;

	addr_serveur.sin_family = AF_INET;
	addr_serveur.sin_port = htons(PORT_SERVEUR_2);
	addr_serveur.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
	if(connect(socket, (SOCKADDR*) &addr_serveur, sizeof(SOCKADDR)))
		return -1;
	if(envoie(fichier_source, socket) < 0)
		return -1;
	err = shutdown(socket, 2);
	return err;
}

int deconnecte_serveur()
{
	return shutdown(Ma_socket, 2);
}
