//:client_queue.h
/****************************************************************************\
 * Le serveur utilise 2 files de clients : une file contenant les nouveaux 
 * clients qui n'ont pas encore été vus par les threads et une autre 
 * contenant les clients déjà vu par les threads (clients en cours). Tout 
 * est caché dans le .c pour faciliter l'utilisation : il y a seulement les 
 * prototypes des fonctions de manipulation
\****************************************************************************/

#ifndef DEF_LISTES
#define DEF_LISTES

#include "client.h"

//Obligatoirement appelée avant toute utilisation des autres fonctions !!!
//Crée et initialise tout le matériel sous-jacent (cf .c)
void init_client_queues();

//renvoie le premier client de la file des clients en cours ou NULL 
//(si file vide ou non accessible au moment de l'appel)
//cette fonction est blocante mais termine (pour un thread) obligatoirement 
//lors de l'arrivée d'un nouveau client ou d'un client en cours)
client pop_client();

//renvoie le premier client de la file des nouveaux clients ou NULL (si file 
//vide ou non accessible au moment de l'appel)
//cette fonction est donc non-blocante
client pop_nouveau();

//ajoute un client en queue de la file des clients en cours
//fonction blocante
void push_client(client c);

//ajoute un client en queue de la file des nouveaux clients
//fonction blocante
void push_nouveau(client c);

//pour faire un peu sérieux : fonction libérant les ressources utilisées
//par la gestion des piles
void term_client_queues();

// pour les tests : affiche le contenu des deux files
void print();

#endif //guard
