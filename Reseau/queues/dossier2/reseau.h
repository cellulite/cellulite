//:reseau.h
/***************************************************************************\
 *Fonctions très utiles pour le réseau, que se soit le serveur ou le client.
\***************************************************************************/

#ifndef DEF_RESEAU
#define DEF_RESEAU

//envoie sur la socket le contenu du fichier fichier_source
//un réponse est attendue de la part du récepteur pour déceler la fin
//empêcher les autres threads de commencer à parler avant la fin de la
//réception
int envoie(char* fichier_source, int socket);

//reception sur la socket et écriture des données reçues dans le fichier
//fichier_destination
//envoie une réponse à l'émetteur une fois le fichier reçu totalement
int recoit(char* fichier_destination, int socket);

#endif //guard
