#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>

#include "config_comm.h"
#include "config_serveur.h"
#include "client.h"
#include "client_queue.h"
#include "reseau.h"

void *Thread_cell(void *arg)
{
	int my_id = (int)arg;
	fprintf(stdout, "SERVEUR> Thread %d ready\n", my_id);
}

void *Threads_waiting(void *arg)
{
	int my_id = (int)arg;
	int erreur = 0;
	client mon_client;
	fprintf(stdout, "SERVEUR> Thread %d ready\n", my_id);
    
	while(1)
	{

		mon_client = pop_nouveau();
		if(mon_client)
		{
//			TRAITEMENT
#ifdef VERBOSE_MODE
			fprintf(stdout, "SERVEUR> T:%d --> decouvre client %d\n", my_id, mon_client->id);
#endif 
			push_client(mon_client);
		}
		mon_client = pop_client();
		if(mon_client)
		{
//			TRAITEMENT
#ifdef VERBOSE_MODE
			fprintf(stdout, "SERVEUR> T:%d --> traite client %d\n", my_id, mon_client->id);
#endif
			erreur = envoie("maCarte", mon_client->socket);
			if(!erreur)
				push_client(mon_client);
			else
			{
				close(mon_client->socket);
				free(mon_client);
#ifdef VERBOSE_MODE
				fprintf(stdout, "SERVEUR> T:%d --> client déconnecté\n", my_id);
#endif
			}
		}
	}
	pthread_exit(EXIT_SUCCESS);
}



int main()
{  
	int nouveau_client;
	pthread_t id[NB_THREADS];
	fd_set rdfs;
	client c;
	struct sockaddr_in addr;
	struct sockaddr csin;
	socklen_t size = sizeof(csin);
	int my_sock = socket(PF_INET, SOCK_STREAM, 0);
	int i;
	pthread_attr_t attr;
	sigset_t set;
	sigaddset(&set, SIGPIPE);
	sigprocmask(SIG_BLOCK, &set, NULL); //masque le signal qui peut être reçu avec read/write
	
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT_SERVEUR_1);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
//	addr.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
	bind(my_sock, (const struct sockaddr*) &addr, sizeof(struct sockaddr_in));
	listen(my_sock, 15);

	init_client_queues();

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for(i = 0; i<NB_THREADS; i++)
	{
		id[i] = i;
		pthread_create(&id[i], &attr, Threads_waiting, (void*)i);
	}
/**/
	while(1)
	{
		FD_ZERO(&rdfs);
		FD_SET(my_sock, &rdfs);
		select(FD_SETSIZE, &rdfs, NULL, NULL, NULL);
		if(FD_ISSET(my_sock, &rdfs))
		{
			nouveau_client = accept(my_sock, &csin, &size);
#ifdef VERBOSE_MODE
			fprintf(stdout, "SERVEUR> nouveau client\n");
#endif
			c = new_client(nouveau_client);
			push_nouveau(c);
		}
	}
/**/
	term_client_queues();
	pthread_exit(NULL);
	return 0;
}
