#ifndef DEF_CONFIG_COMM
#define DEF_CONFIG_COMM

#define TAILLE_BUFF 1024
#define PORT_SERVEUR_1 1234
#define PORT_SERVEUR_2 1235
#define ADRESSE_SERVEUR "127.0.0.1"
#define FORMAT AF_INET

#endif //guard
