//:reseau.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "reseau.h"

//taille du buffer utilisé pour la lecture et l'écriture
#define LONGUEUR 1024
#define ACK "Bien reçu"
#define LONGUEUR_ACK 10

int envoie(char* fichier_source, int socket)
{
	char buffer[LONGUEUR];
	int n_env, n_lu;
	fd_set ens_rec;
	int fd;
	struct timeval timeout; //quelle valeur mettre ??
	timeout.tv_sec = 1; //secondes
	timeout.tv_usec = 0; //microsecondes

	FD_ZERO(&ens_rec);
	FD_SET(socket, &ens_rec);
	fd = open(fichier_source, O_RDONLY);
	if(fd < 0)
	{
		fprintf(stderr, "Erreur dans envoie : echec de l'ouverture de %s\n", fichier_source);
		return -1;
	}
	do
	{
		n_lu = read(fd, buffer, LONGUEUR);
		if(n_lu < 0)
			return -1;
		n_env = write(socket, buffer, n_lu);
		if(n_env != n_lu)
			return -1;
	}while(n_lu == LONGUEUR);
	close(fd);
	if(select(FD_SETSIZE, &ens_rec, NULL, NULL, &timeout) <= 0)
	{
		fprintf(stderr, "Erreur dans envoie : timeout dépassé\n");
		return -1;
	}
	if(!FD_ISSET(socket, &ens_rec))
		return -1;
	n_lu = read(socket, buffer, LONGUEUR);
//	if(n_lu != LONGUEUR_ACK)
//		return -1;
#ifdef VERBOSE_MODE
	buffer[n_lu] = '\0';
	fprintf(stdout, "Ack reçu : %s\n", buffer);
#endif
	return (n_lu <= 0); //on ne vérifie pas le contenu du message (ne semble pas important)
}

int recoit(char* fichier_destination, int socket)
{
	char buffer[LONGUEUR];
	int n_rec, n_ecr;
	fd_set ens_rec;
	int fd;
	struct timeval timeout; //quelle valeur ?
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	fd = open(fichier_destination, O_WRONLY | O_CREAT | O_TRUNC, S_IREAD | S_IWRITE);
	if(fd < 0)
	{
		fprintf(stderr, "Erreur dans recoit : echec de l'ouverture de %s\n", fichier_destination);
		return -1;
	}
	FD_ZERO(&ens_rec);
	FD_SET(socket, &ens_rec);
	if(select(FD_SETSIZE, &ens_rec, NULL, NULL, &timeout) <= 0)
	{
		fprintf(stderr, "Erreur dans recoit : timeout dépassé\n");
		return -1;
	}
	if(!FD_ISSET(socket, &ens_rec))
		return -1;
	do
	{
		n_rec = read(socket, buffer, LONGUEUR);
		if(n_rec < 0)
			return -1;
		n_ecr = write(fd, buffer, n_rec);
		if(n_ecr != n_rec)
			return -1;
	}while(n_rec == LONGUEUR);
	close(fd);
	n_ecr = write(socket, ACK, LONGUEUR_ACK);
#ifdef VERBOSE_MODE
	fprintf(stdout, "Ack envoyé\n");
#endif
	return (n_ecr == LONGUEUR_ACK);
}

