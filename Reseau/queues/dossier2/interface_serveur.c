#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>
#include "config_comm.h"
#include "config_serveur.h"
#include "interface_serveur.h"

int init_serveur()
{
	int my_sock;
	struct sockaddr_in addr;
	sigset_t set;
	sigaddset(&set, SIGPIPE);
	sigprocmask(SIG_BLOCK, &set, NULL); //masque le signal qui peut être reçu avec read/write
	my_sock = socket(PF_INET, SOCK_STREAM, 0);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT_SERVEUR_1);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
//	addr.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
	bind(my_sock, (const struct sockaddr*) &addr, sizeof(struct sockaddr_in));
	listen(my_sock, 15);

	init_client_queues();

	return my_socket;
}
