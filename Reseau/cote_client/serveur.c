#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "config_comm.h"
#include "config_serveur.h"

int main()
{  
    int nouveau_client, i, nb_lines;
    int my_sock = socket(PF_INET, SOCK_STREAM, 0);
    fd_set rdfs;
    FILE *stream;
    struct sockaddr_in addr;
    struct sockaddr csin;
    socklen_t size = sizeof(csin);
    int n_lu;
    char buffer[TAILLE_BUFF];
	
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT_SERVEUR);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
//	addr.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
    bind(my_sock, (const struct sockaddr*) &addr, sizeof(struct sockaddr_in));
    listen(my_sock, 15);
    
    while(1)
    {
	FD_ZERO(&rdfs);
	FD_SET(my_sock, &rdfs);
	select(FD_SETSIZE, &rdfs, NULL, NULL, NULL);
	if(FD_ISSET(my_sock, &rdfs))
	{
	    nouveau_client = accept(my_sock, &csin, &size);
	    stream = fdopen(nouveau_client, "r"); 
	    fscanf(stream,  "%d\n", &nb_lines);
	    fprintf(stderr, "nb_lines : %d\n", nb_lines);
	    i = 0;
	    do
	    {
		fgets(buffer, 10, stream);
		if(buffer[strlen(buffer) -1] == '\n') i++;
		fprintf(stderr, " J'archive : %s ", buffer);
	    }
	    while(nb_lines > i); 
	    printf("\n");
	    fclose(stream);
		   
	}
    }
    return 0;
}
