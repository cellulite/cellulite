#ifndef _DEF_RENDERER
#define _DEF_RENDERER

#include <SDL/SDL.h>
#include "cEye.h"

class cRenderer
{
public:
	static bool _init;

	cRenderer(int width, int heigth);
	~cRenderer();
	void update(int cells[][5], int nb_cells);
	cRenderer& set_user(int user) { _user = user; return *this; };
	cRenderer& set_mapX(int mapX) { _mapX = mapX; _eye->set_mapX(mapX); return *this; };
	cRenderer& set_mapY(int mapY) { _mapY = mapY; _eye->set_mapY(mapY); return *this; };
	cRenderer& get_mouse_motion(SDL_MouseMotionEvent &event);
	cRenderer& get_mouse_button(SDL_MouseButtonEvent &event);
	cRenderer& get_keyboard(SDL_KeyboardEvent &event);

private:
	int _user;
	//angle de perspective, ratio width/heigth, distance min/max de visibilité
	double fovy, aspect, zNear, zFar;
	//paramètres du point de vue
	const static double _rad = 0.3;

	int _mapX, _mapY;

	int _width, _heigth;
	SDL_Surface* _screen;
	void dessin_limites(double z_min, double z_max);
	void dessin(int cells[][5], int nb_cells);
	void dessin_cell(double x, double y, double z, double rad, int r, int b, int v);

	cEye *_eye;
};

#endif //guard
