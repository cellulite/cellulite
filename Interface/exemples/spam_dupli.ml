partage : vars () args ()
let dir  = See() in
SendEnergy(energy/20,dir);;
	  

(* tente de se dupliquer *)
try_duplicate : vars () args (seuil,part,dir)
  if age > 900
  then
    partage()()
  else
    if energy < seuil
    then
      Skip
    else
      let occup = See() in
      if (not (occup band dir))
      then
	Duplicate(energy/part,dir,try_duplicate()(seuil, part,dir))
      else
	if (not (occup band NextDir(dir)))
	then
	  Duplicate(energy/part,NextDir(dir),try_duplicate()(seuil, part,dir))
	else
	  if (not (occup band PrevDir(dir)))
	  then
	    Duplicate(energy/part,PrevDir(dir),try_duplicate()(seuil, part,dir))
	  else
	    if (not (occup band NextDir(NextDir(dir))))
	    then
	      Duplicate(energy/part,NextDir(NextDir(dir)),try_duplicate()(seuil, part,dir))
	    else
	      if (not (occup band PrevDir(PrevDir(dir))))
	      then
		Duplicate(energy/part,PrevDir(PrevDir(dir)),try_duplicate()(seuil, part,dir))
	      else
		Skip;;


souche : vars (etat) args (seuil,partage,seuil_souche)
if energy < seuil_souche
then Skip
else
  if etat = 0
  then
    etat := 1;
    try_duplicate()(seuil,partage,N)
  else 
   if etat = 1
   then
     etat := 2;
    try_duplicate()(seuil,partage,S)
   else
     if etat = 2
     then
    etat := 3;
    try_duplicate()(seuil,partage,E)
    else
     if etat = 3
     then    
       etat := 0;
    try_duplicate()(seuil,partage,O)
else Skip;;

@souche(0)(300,2,200)
