from subprocess import *
import subprocess

sending_program = "./client_sending"
receiving_program = "./client_receiving"
subscribing_program = "./client_subscribe"
deleting_program = "./client_deleting"

def processus_cell(file_name, _login, _pass):
    global sending_program
    retour = subprocess.call([sending_program, file_name, _login, _pass])
    if(retour != 0):
        return 1
    else :
        return 0

def send_subscribe(_login, _pass):
    global subscribing_program
    retour = subprocess.call([subscribing_program, _login, _pass])
    return retour;

def processus_del(num_cell, _login, _pass):
    global deleting_program
    print deleting_program+" "+_login+" "+_pass
    return subprocess.call([deleting_program, num_cell, _login, _pass])

#def receive_proc():
#    subprocess.Popen([receiving_program])
    
