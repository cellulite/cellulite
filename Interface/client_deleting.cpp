#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "config_comm_s.h"
#define EXIT_FAILURE 1
#define ACK_SIZE 7
#define LONGUEUR 1024

int main (int argc, char *argv[]){
// FOr testing    
//     struct hostent *hostinfo = NULL;
    struct sockaddr_in addr;
//    int length;
    //nb_lu, nb_wr; // adresse du serveur
    char buffer[TAILLE_BUFF] = "delete ";
//    char buffer_temp[TAILLE_BUFF] = "";
    //  int stream;
    int sock = socket(PF_INET, SOCK_STREAM, 0);
    fd_set ens_rec;
//    struct stat fstat;
//    struct timeval timeout; //quelle valeur mettre ??
//    timeout.tv_sec = 10; //secondes
//    timeout.tv_usec = 0; //microsecondes


    fprintf(stderr, "%d\n", argc);

    if(argc != 4)
    {
	fprintf(stderr, "Usage : %s [numero de famille] [login] [password]\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    memset(& addr, 0, sizeof(struct sockaddr_in));
    addr.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
    addr.sin_port = htons(PORT_SERVEUR);
    addr.sin_family = AF_INET;
    
    if(connect(sock,(struct sockaddr *) &addr, sizeof(struct sockaddr))){
	fprintf (stderr, "Connexion failed.\n");
	exit(EXIT_FAILURE);
    }

    FD_ZERO(&ens_rec);
    FD_SET(sock, &ens_rec);

    strcat(strcat(strcat(strcat(strcat(strcat(buffer, argv[2]), " "), argv[3]), " "), argv[1]), "\n");
    fprintf(stderr, "buff : %s\n", buffer);
    write(sock, buffer, strlen(buffer));
    
    close(sock);
    return 0; 
}
