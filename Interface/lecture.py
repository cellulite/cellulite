import subprocess
import os
import signal
from subprocess import *
process_receiver_name = "./client_receiving"
score = []

def init_receiving(_login, _password):
    global file_desc, process_receiver_name, process_receiver
    arg = process_receiver_name+' '+_login+' '+_password
    print arg
    process_receiver = subprocess.Popen(arg, shell = True, stdout=subprocess.PIPE, preexec_fn=os.setsid)
    file_desc = process_receiver.stdout
#    print "on en est la et tudubuen\n"
    if(process_receiver.poll() == None):
#        print "en attente d'une lecture de ligne\n"
        ligne = file_desc.readline()
        mots = ligne.split()
        print "---->premier mot recu :"+mots[0]
 #       print "python recoit : "+ mots[0]+"\n"
        if(mots[0] == "badauth"):
            return -2
        if(mots[0] == "failedconnect"):
            return -1
        return int(mots[0])
    else:
        return process_receiver.poll()

def read_score():
    global file_desc, score, process_receiver
    ligne = file_desc.readline()
    if((process_receiver.poll() != None)):
        kill_process()
        return -1
    if(ligne == ""):
        kill_process()
        return -2
    score = []
    mots = ligne.split()
    nb_players = int(mots[0])
    nb_family = int(mots[1])
    for i in range(nb_players):
        ligne = file_desc.readline()
        mots = ligne.split()
        curr_pl = mots[0]
        score.append([curr_pl])
        ligne = file_desc.readline()
        mots = ligne.split()
        for j in range(nb_family):
            score[i].append(mots[j])
    return score


def kill_process():
    global process_receiver
    try:
        process_receiver 
    except NameError :
        return
    else :
        if(process_receiver.poll() == None):
            os.killpg(process_receiver.pid, signal.SIGTERM)
