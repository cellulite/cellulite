import subprocess
import os
import signal
from subprocess import *
process_receiver_name = "./client_receiving"
cells = []

def save_line(line_to_save, i):
    global cells
    mots = line_to_save.split()
#    print "---> "+line_to_save+"\n"
    cells.append([int(mots[3]), int(mots[4]), mots[0], int(mots[2])])
#    print "ici longueur : "+str(len(cells))+"\n"

def init_receiving():
    global file_desc, process_receiver_name, process_receiver, file_temp_w, file_temp_r
    file_temp_w = open("stdout_temp", "a+")
    process_receiver = subprocess.Popen(process_receiver_name, shell = True, stdout=file_temp_w, preexec_fn=os.setsid)
    file_temp_r = open("stdout_temp", "a+")
#    file_desc = process_receiver.stdout

def read_map():
    global file_desc, cells, file_temp_r
    ligne = file_temp_r.readline()
    cells = []
    while(ligne == ""):
#        file_temp_r.close()
 #       file_temp_r = open("stdout_temp", "a+")
        ligne = file_temp_r.readline()    
    if(ligne == "ERREUR"):
        print "ligne :"+ligne
        return -1
 #   print "/////> "+ligne
    mots = ligne.split()
    for i in range(int(mots[0])):
        ligne = file_temp_r.readline()
        while(ligne == ""): 
            ligne = file_temp_r.readline()    
        save_line(ligne, i)
    return cells


def kill_process():
    global process_receiver
    try:
        process_receiver 
    except NameError :
        return
    else :
        os.killpg(process_receiver.pid, signal.SIGTERM)
        if(os.path.isfile("stdout_temp")):
            os.remove("stdout_temp")
        
