#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "config_comm_r.h" //Contient le port et l'adresse du serveur
#include "interface_client.h"

#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

static SOCKET Ma_socket;
static SOCKADDR_IN Serveur;
static char Buffer[TAILLE_BUFF];
static FILE *stream;



int main (int argc, char *argv[]){
    char ack[8];
    if(argc != 3)
	return -1;
    
    if((Ma_socket = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
	fprintf(stderr, "binding socket failed\n");
	return -1;
    }
    Serveur.sin_family = AF_INET;
    Serveur.sin_port = htons(PORT_SERVEUR);
    Serveur.sin_addr.s_addr = inet_addr(ADRESSE_SERVEUR);
    if(connect(Ma_socket, (SOCKADDR*) &Serveur, sizeof(SOCKADDR)))
    {
	fprintf(stderr, "Connection socket failed\n");
	return -1;
    }

    if(write(Ma_socket, (void*)"inscript\n", 8) < 0){
	fprintf(stderr, "Write socket failed\n");
	return -1;
    }

    strcat(strcat(strcat(strcat(Buffer, argv[1]), " "), argv[2]), "\n");
    fprintf(stderr, "-->%s\n", Buffer);
    if(write(Ma_socket, Buffer, strlen(Buffer)) < 0){
	fprintf(stderr, "Write socket failed\n");
	return -1;
    }
    stream = fdopen(Ma_socket, "r+");
    fgets(ack, 8 ,stream);
    fprintf(stderr, "return value : -%s-\n", ack);
    if(strcmp(ack, "registr") != 0)
	return -2;

    fclose(stream);
    shutdown(Ma_socket, 2);

    return 0;
}
