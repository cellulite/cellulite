#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
#include <pthread.h>
#include <SDL/SDL.h>
#include "cRenderer.h"
//#include "interface_client.h"
using namespace std;

string filename ("map");

int main()
{
/*	if(connecte_serveur("admin", "admin") < 0)
	{
		return 1;
	}
*/
	cRenderer *render = new cRenderer(800, 600);
	render->set_mapX(50);
	render->set_mapY(30);
	SDL_Event event;
	bool stop = false;
	int (*cells)[5];
	int nb_cells;

	ifstream map (filename.c_str());
	if(! map.is_open())
	{
		cerr << "Impossible d'ouvrir le fichier " << filename << endl;
		return 1;
	}
	map >> nb_cells;
	cells = new int [nb_cells][5];
	for(int i = 0 ; i < nb_cells ; ++i)
	{
		map >> cells[i][0] >> cells[i][1] >> cells[i][2] >> cells[i][3] >> cells[i][4];
	}
	map.close();

	while(!stop)
	{
		for(int i = 0 ; i < 20 && SDL_PollEvent(&event) ; ++i)
		{
			switch(event.type)
			{
				case SDL_QUIT:
					stop = true;
					break;
				case SDL_MOUSEMOTION:
					render->get_mouse_motion(event.motion);
					break;
				case SDL_MOUSEBUTTONUP:
				case SDL_MOUSEBUTTONDOWN:
					render->get_mouse_button(event.button);
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym)
					{
						case SDLK_ESCAPE:
							stop = true;
							break;
						default:
							render->get_keyboard(event.key);
							break;
					}
					break;
			}
		}
//		i_receive(&cells, nb_cells);
		render->update(cells, nb_cells);
	}

//	deconnecte_serveur();
	delete [] cells;

	return 0;
}
