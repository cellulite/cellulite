#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include "cEllipsoid.h"
using namespace std;

#define _WIDTH 800
#define _HEIGTH 600

int main()
{
	SDL_Event event;
	cEllipsoid sphere(1.0, 2.0, 5.0);
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
		return 1;
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_Surface* _screen = SDL_SetVideoMode(_WIDTH, _HEIGTH, 32, SDL_OPENGL);
	if(!_screen)
	{
		cerr << "Echec de la création de la fenêtre : " << SDL_GetError() << endl;
		exit(EXIT_FAILURE);
	}
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40.0, (double)_WIDTH / _HEIGTH, 1.0, 100.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(5.0, 5.0, 8.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);

	glColor3ub(255, 0, 0);
	sphere.generate(9, 9);
	sphere.draw(0.0, 0.0, 0.0);

	glFlush();
	SDL_GL_SwapBuffers();
	bool stop = false;
	while(!stop)
	{
		if(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_QUIT:
					stop = true;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym)
					{
						case SDLK_ESCAPE:
							stop = true;
							break;
					}
			}
		}
	}
	return 0;
}
