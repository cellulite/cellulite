#ifndef _DEF_ELLIPSOID
#define _DEF_ELLIPSOID

class cEllipsoid
{
public:
	cEllipsoid(double a, double b, double c);
	~cEllipsoid();
	int generate(int nb_latitudes, int nb_longitudes); //Returns 0 if ok
	int draw(double x, double y, double z); //(x, y, z) defines the center, returns 0 if ok
	

private:
	void normalize_and_put(double x, double y, double z);
	double *_x, *_y, *_z; //Generated points if center = (0, 0, 0)
	double _a, _b, _c; //Ellipsoid caracteristics
	int _nb_latitudes, _nb_longitudes; //Numbers of slices (without top and bottom)
};

#endif
