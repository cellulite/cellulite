#ifndef DEF_POSITION		/* pour ne charger qu'une fois ce .h */
#define DEF_POSITION

#include <iostream>
#include "direction.h"

/* Il faut noter que l'origine (<0,0>) se trouve en haut à gauche (Nord-Ouest) */
class Position
{
	//affichage sympa via cout << pos
	friend std::ostream& operator<< (std::ostream& output, const Position& pos);

public:
	//dimensions de la carte : doivent être initialisées dans un .cpp (actuellement position.cpp)
	static int x_max, y_max; // Attribué dans le position.cpp

	//constructeurs
	//initialise à (0, 0)
	Position();
	//initialise à (x_, y_)
	Position(int x_, int y_);
	
	//opérateurs utiles
	//attention, pour l'instant, la carte est un tore
	Position& operator= (const Position& pos); /* permet d'utiliser = */
	//si j'ai bien compris, le premier '&' permet d'enchainer les '=' dans tous les sens
	//en fait, "pos1 = pos2" renverra pos1 donc on pourrait par exemple écrire "(pos1 = pos2).zero()"
	//si zero était une méthode de Position
	//le second '&' évite de dupliquer la position passée en argument
	//const précise que pos n'est pas modifié
	Position operator+ (const Position& pos);  /* permet d'additioner des positions */
	//permet de faire pos1 = pos2 + S_O
	Position operator+ (Direction dir); /* permet de calculer une position décalée d'une direction */
	Position& operator+= (const Position& pos); /* la même avec += d'une position */
	Position& operator+= (Direction dir);	    /* la même avec += d'une direction */

	/* Accesseurs pour l'affichage */
	int x() const;
	int y() const;
	
	/*  comparateur de position (principalement pour pouvoir construire des set de positions */
	bool operator < (const Position&) const;

private:
	// coordonnées
	int p_x, p_y;
};

#endif //guard
