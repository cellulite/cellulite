#ifndef ENV_H_INCLUDED		/* pour ne charger ce .h qu'une fois */
#define ENV_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <queue>
#include <map>
#include <vector>
#include "cell.h"
#include "Log.h"


/**********************************************/
/* Ce fichier définit la signature des        */
/* objets utilisés par la simulation en l'ADN */
/*                                            */
/**********************************************/


/***********************/
/* OBJET ENVIRONNEMENT */
/***********************/

typedef std::vector<Cell> vect_de_cell;
typedef std::vector<Cell*> vect_de_cell_2;
/* En plus de la classe cell, cell.h charge le type assign:
   le type de la structure symbolisant une assignation (x,y) : rang x dans data -> valeur nouvelle y
   typedef std::vector<std::complex <int> > assign;
*/

class Env
{
  // -- Affichage pour débugger -- //
  // Pour afficher l'état de l'environnement (on affiche toutes les cellules sous la forme d'une "forêt généalogique")
  // Attention: ça peut prendre beaucoup de temps, c'est pas du tout optimal mais on ne s'en sert que pour débugger
  friend std::ostream& operator<< (std::ostream& output, const Env& e);
  // -- Affichage pour client -- //
  friend std::string affiche_terrain(Env& e);

 public:
  // -- Constructeur -- //
  Env();
  // -- Déconstructeur -- //
  ~Env();

  // -- Méthodes pour la gestion de l'environnement par le serveur -- //
  // ---------------------------------------------------------------- //
  // -- Numéro de famille disponible pour un client donné (-1 si aucun)
  int get_free_family(int proprietaire_);
  // -- Ajout d'une cellule dans l'environnement: ne sera plus utile à terme!! -- //
  bool add_cell(Cell* cell, Position pos);
  // -- Ajout d'une cellule dans l'environnement avec chargement et placement aléatoire -- //
  bool add_cell_auto(const char *nom_fichier, int proprietaire_);
  // -- Ajout d'une cellule dans l'environnement avec chargement et info de la mere -- //
  Cell* add_cell_pos(Cell* mother, const char* nom_ss_fct, int *tableau_args, int* tableau_vars, int debut, assign ass, int taille_code, int nrj_, Position pos);
  // -- Suppression d'une cellule de l'environnement -- //
  bool rm_cell(Cell* cell, int flag); /* flag = 1: on supprime la case dans les_cell, flag = 0: on remplace le pointeur par NULL (-> pas de décalage d'indice) */
  // -- Suppression d'une famille de cellules -- //
  void rm_famille(int proprietaire, int famille);
  // -- Supression de toutes les cellules -- //
  void clear_all_cell();
  // -- donne l'adresse de la cellule d'une case ou NULL s'il n'y a personne -- //
  Cell* findCell(Position pos);
  // -- met à jour les positions de chaque cellule (déplacements bufferisés) -- //
  void update_pos();
  // -- faire calculer toutes les cellues --//
  void calc_tour();

  // -- Méthodes pour la sauvegarde/chargement de l'environnement -- //
  // --------------------------------------------------------------- //
  void save_etat (char* fichier_sauvegarde);
  void load_etat (char* fichier_sauvegarde);

  // -- la présence ou non de cellules sur les cases -- //
  // -------------------------------------------------- //
  void voir(Cell* myself, bool tab_vide[]); // renvoie un vecteur de direction
  /* Attention: on suppose que tab_vide est déjà initialisé à un tableau statique de taille 8 */

  int see(Cell* myself); //renvoit un int correspondant (au sens du compilateur donc de mon_action()) à voir
  
  // -- Méthodes utilisées par les cellules -- //
  // ----------------------------------------- //
  // -- Constantes de l'environnement -- //
  int E_init(); // "énergie initiale"
  int E_sol(); // "énergie du soleil"
  int E_mort(); // seuil minimal avant de mourir (si < alors mort :( )
  int A_mort(); // âge maximal ( si > alors mort :( )
  void fix_A_mort(int new_A);	/* pour changer l'age maximal (A_mort) ne servira à rien à terme */
  
  // -- les fonctions de couts des actions -- //
  int costSendMessage(bool vect_dir[8], Message mess);
  int costSendEnergy(bool vect_dir[8], int quantite_nrj);
  int costDuplicate(int taille_ss_fct, int fwEnergy);
  int costMove();		/* c'est une cste */ // à passer en anglais quand vous aurez le temps (message de thomas)
  
  // -- les fonctions réalisant (si c'est possible) les actions demandés par les cellules */
  void appelPreambule(Cell *myself, assign ass); /* vérifie que la mémoire de la fonction appellée a été assignée, le fait sinon */
  void sendMessage(Cell *myself, bool vect_dir[8], Message message);
  void sendEnergy(Cell *myself, bool vect_dir[8], int quantite_nrj); 
  void Duplication(Cell *myself, char* nom_ss_fct, int taille_ss_fct, Direction dir, int fwEnergy, int *tableau_valeurs_args, int *tableau_indices_vars, int debut, assign ass);
  //  les deux premiers tableaux pour que le nouveau main ait du sens, le ass pour assigner la mémoire de CETTE sous-fonction 
  void Deplacement(Cell *myself, Direction dir); //0 <= direction < 8 
  // Rappel: les déplacements sont bufferisés (le déplacement ne se fait qu'à la fin du tour)
  
  // -- Outils pour debbugger -- //
  bool swap_pos(Cell* cell, Position pos); /* permet de fixer la position d'une cellule existante */

  // -- cellules à traiter -- //
  vect_de_cell_2 les_cell;		/* les cellules à traiter dans cette ordre */
  
 private:
  // -- Constantes de l'environnement -- //
  int p_E_init; // "énergie initiale"
  int p_E_sol; // "énergie du soleil"
  int p_E_mort; // seuil minimal
  int p_A_mort; //âge maximal

  // -- Structures internes pour gérer les positions et déplacements -- //
  std::map<Position,Cell*> map_pos;	/* map les positions vers les cellules (unique) pas de clé si pas de cellule */
  std::map<Position,vect_de_cell_2> next_pos; /* map les positions vers un vect de cell: pour gérer les conflits */

  // -- Structures internes pour calculer les scores -- //
  std::map<int,int**> joueur_scores; /* permet de calculer les scores */
  /* un mapping des identifiants de joueurs (entier) vers un tableau de familles, chaque case de ce vecteur contien un tableau a deux cases: score et nb de cellules mortes */
  
  // -- Structures internes pour savoir quelles sont les numéros de familles possibles pour les propriétaires -- //
  std::map<int,std::queue<int> > joueur_fami_libres; /* une file des numéros de familles pas encore pris pour chaque joueur */
};

#endif	// guard
