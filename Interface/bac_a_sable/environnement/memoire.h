#ifndef DEF_MEMOIRE		/* pour ne charger qu'une fois ce .h */
#define DEF_MEMOIRE

#include <iostream>

class Memoire
{
	//pour afficher la mémoire avec `cout << mem;`
	friend std::ostream& operator<< (std::ostream& output, const Memoire& mem);

public:
	//constructeur
	Memoire();

	//destructeur
	~Memoire();

	//pour choisir la taille de la mémoire une fois la mémoire créée.
	//ces deux actions sont séparées pour permettre le réglage de la 
	//taille dans une fonction du fichier compilé.
	//!! attention, on ne peut choisir la taille qu'une seule fois :
	//ce n'est pas comme resize pour les vecteurs !!
	Memoire& set_taille(int nb_vars_);

	//permet d'accéder (écriture ou lecture grace au '&') directement à une case mémoire via mem[indice]
	//on n'a donc pas besoin d'utiliser mem.data[indice]
	//attention, en cas de segfault, on affiche les raisons
	//mais le signal est tout de même lancé.       
	//Il faut noter que cette méthode gère en interne les erreurs (rang incompatible etc.)
	int& operator[] (int indice_);

	/* Donne le nombre de variables */
	int nb_vars() const;
private:
	int p_nb_vars;		/* le nombre de variables */
	int *data;		/* le tableau statique contenant les cases mémoires (entiers) */

};

#endif //guard
