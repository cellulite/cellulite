#ifndef LOG_H_INCLUDED		/* pour ne charger ce .h qu'une fois */
#define LOG_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string>

/************************************/
/* Ce fichier définit la signature  */
/* de la classe gérant les erreurs  */
/*                                  */
/************************************/

class Log
{
  // -- Affichage du log des erreurs -- //
  friend std::ostream& operator<< (std::ostream& output, const Log& log);
  friend Log& operator<< (Log& log, std::string mess);
  friend Log& operator<< (Log& log, int entier);
  
 public:
  // -- Constructeur -- //
  Log();
  // -- Déconstructeur -- //
  ~Log();
  // -- Supprimer le log des erreurs -- //
  void rm ();
  // -- Ajouter un string au log -- //
  void add (std::string mess);

 private:
  std::string message;
};

#endif  // guard
