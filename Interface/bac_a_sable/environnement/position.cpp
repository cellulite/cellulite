#include <iostream>
#include "position.h"
using namespace std;

// issu d'un .config ATTENTION: vérifier compatibilité avec env.cpp
int Position::x_max = 50;
int Position::y_max = 50;

Position::Position()
: p_x	(0)
, p_y	(0)
{
}

Position::Position(int x_, int y_)
: p_x	(x_)
, p_y	(y_)
{
}

int Position::x() const { return p_x; }
int Position::y() const { return p_y; }

Position& Position::operator= (const Position& pos)
{
	p_x = pos.p_x;
	p_y = pos.p_y;
	return (*this);
}

Position Position::operator+ (const Position& pos)
{
	int x_ = (p_x + pos.p_x) % x_max;
	int y_ = (p_y + pos.p_y) % y_max;
	Position pos1(x_, y_);
	return pos1;
}

Position Position::operator+ (Direction dir)
{
	int x_ = 0, y_ = 0;
	switch(dir)
	{
		case N_O:
			x_ = (x_max + p_x - 1) % x_max;
			y_ = (y_max + p_y - 1) % y_max;
			break;
		case N:
			x_ = (x_max + p_x - 1) % x_max;
			y_ = p_y;
			break;
		case N_E:
			x_ = (x_max + p_x - 1) % x_max;
			y_ = (p_y + 1) % y_max;
			break;
		case O:
			x_ = p_x;
			y_ = (y_max + p_y - 1) % y_max;
			break;
		case E:
			x_ = p_x;
			y_ = (p_y + 1) % y_max;
			break;
		case S_O:
			x_ = (p_x + 1) % x_max;
			y_ = (y_max + p_y - 1) % y_max;
			break;
		case S:
			x_ = (p_x + 1) % x_max;
			y_ = p_y;
			break;
		case S_E:
			x_ = (p_x + 1) % x_max;
			y_ = (p_y + 1) % y_max;
			break;
	}
	Position pos1(x_, y_);
	return pos1;
}

Position& Position::operator+= (const Position& pos)
{
	return (*this = *this + pos);
}

Position& Position::operator+= (Direction dir)
{
	return (*this = *this + dir);
}

bool Position::operator< (const Position& pos) const {
  return ((p_x < pos.x()) || (p_x == pos.x() && p_y < pos.y()));
};

ostream& operator<< (ostream& output, const Position& pos)
{
	output << "(" << pos.p_x << ", " << pos.p_y << ")";
	return output;
}
