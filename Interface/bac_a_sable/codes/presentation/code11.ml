serpent : vars (dir, pasRestants, b) args (cote, sens)
  if b = 0 then (* Duplication *)
    {
      b := 1;
      if See() band dir then (* Quelqu'un devant *)
	{
	  SendEnergy(energy / 2, dir)
	}
      else (* Personne devant *)
	{
	  if energy > costDuplicate(50, serpent) then
	    {
	      if pasRestants = 1 then (* On arrive devant un coin *)
		{
		  if sens = 1 then (* sens horaire *)
		    {
		      Duplicate(50, dir, serpent(NextDir(NextDir(dir)), cote - 1, 1)(cote, sens))
		    }
		  else (* sens trigo *)
		    {
		      Duplicate(50, dir, serpent(PrevDir(PrevDir(dir)), cote - 1, 1)(cote, sens))
		    }
		}
	      else (* En ligne droite *)
		{
		  Duplicate(20, dir, serpent(dir, pasRestants - 1, 1)(cote, sens))
		}
	    }
	  else
	    {
	      Skip
	    }
	}
    }
(*  else if b = 1 then (* Déplacement *)
    {
      b := 2;
      if pasRestants = 1 then (* On va arriver dans un coin *)
	{
	  pasRestants := cote - 1;
	  if sens = 1 then (* sens horaire *)
	    {
	      let tmp = dir in
		dir := NextDir(NextDir(dir));
		Move(tmp)
	    }
	  else (* sens trigo *)
	    {
	      let tmp = dir in
		dir := PrevDir(PrevDir(dir));
		Move(tmp)
	    }
	}
      else (* Milieu d'une ligne droite *)
	{
	  pasRestants := pasRestants - 1;
	  Move(dir)
	}
    }
*)  else (* b > 1 : On accumule de l'énegie *)
    {
      b := (b + 1) % 20;
      Skip
    }
;;

propulseur : vars(dist) args(dirstart, sens, cote)
  if(dist = 0) then
    {
      if (energy > costDuplicate(100, serpent)) then
	{
	  if sens = 1 then (* sens horaire*)
	    {
	      Duplicate(100, NextDir(NextDir(dirstart)), serpent(NextDir(NextDir(dirstart)), (cote - 1) / 2 - 1, 0)(cote, sens))
	    }
	  else (* sens trigo *)
	    {
	      Duplicate(100, PrevDir(PrevDir(dirstart)), serpent(PrevDir(PrevDir(dirstart)), (cote - 1) / 2 - 1, 0)(cote, sens))
	    }
	}
      else
	{
	  Skip
	}
    }
  else
    {
      if (energy > costMove() and ((See() band dirstart) = 0)) then
	{
	  dist := dist - 1;
	  Move(dirstart)
	}
      else
	Skip (* éventuellement plus tard envoyer un messager d'alerte vers OpDir(dirstart) pour dire qu'il a trouvé un obstacle *)
    }
;;

garde : vars (att, dirGo) args (dirSur, dirPr, pre)
  if att then
    {
      att := 0;
      Move(OpDir(dirGo))
    }
  else if (age >= (Age_death - pre)) then
    {
      SendEnergy(((100 * (energy - 5)) / 105) - 1, dirPr)
    }
  else
    {
      dirGo := (See() band dirSur);
      if dirGo then
	{
	  SendMessage(1, dirPr)
	  && Skip
	  && att := 1; Move(dirGo)
	}
      else 
	{
	  Skip
	}
    }
;;

centre : vars (dNE, dNO, dSO, dSE, init, look, cpt, dup, serp1) args (dir, pre)
  if ((age >= (Age_death - pre)) and (dup = 0)) then
    {
      dup := 1;
      Move(OpDir(dir))
      && Duplicate((10 *(energy - 40)) / 11, dir, centre(1, 1, 1, 1, 1, 0, cpt, 0, serp1)(dir, pre))
    }
  else if init = 1 then
    {
      if cpt = 10 then
	{
	  cpt := cpt + 1;
	  Duplicate(energy / 16, NE, garde(0, O)(E + NE + N, SO, 3))
	}
      else if cpt = 20 then
	{
	  cpt := cpt + 1;
	  Duplicate(energy / 16, NO, garde(0, O)(N + NO + O, SO, 3))
	}
      else if cpt = 30 then
	{
	  cpt := cpt + 1;
	  Duplicate(energy / 16, SO, garde(0, O)(O + SO + S, SO, 3))
	}
      else if cpt = 40 then
	{
	  init := 0;
	  cpt := 0;
	  Duplicate(energy / 16, SE, garde(0, O)(S + SE + E, SO, 3))
	}
      else
	{
	  cpt := cpt + 1;
	  Skip
	}
    }
  else if Exists(mess, dir, dir band (NE + SE + SO + NO)) then
    {
      SendEnergy(energy / 16, dir band (NE + SE + SO + NO))
    }
  else if cpt > 8 then
    {
      cpt := 0;
      look := See();
      if (((look band NE) = 0) and dNE) then 
	{
	  Duplicate(energy / 16, NE, garde(0, O)(E + NE + N, SO, 3))
	}
      else if (((look band NO) = 0) and dNO) then 
	{
	  Duplicate(energy / 16, NO, garde(0, O)(N + NO + O, SO, 3))
	}
      else if (((look band SO) = 0) and dSO) then 
	{
	  Duplicate(energy / 16, SO, garde(0, O)(O + SO + S, SO, 3))
	}
      else if (((look band SE) = 0) and dSE) then 
	{
	  Duplicate(energy / 16, SE, garde(0, O)(S + SE + E, SO, 3))
	}
      else
	{
	  Skip
	}
    }
  else
    {
      cpt := cpt + 1;
      if (serp1 = 0 and energy > 1000) then
	{
	  serp1 := 1;
	  Duplicate(500, N, propulseur(6)(N, 1, 15))
	}
      else
	{
	  Skip
	}
    }
;;

chenille : vars (dir, dup, num) args (pre, pas)
  if (num > pas) then
    {
      if (age = (Age_death - pre)) then
	{
	  Duplicate((10 * (energy - 40)) / 11, dir, centre(1, 1, 1, 1, 1, 0, 0, 0, 0)(dir, 3))
	}
      else 
	{
	  Skip
	}
    }
  else if dup then
    {
      if age = (Age_death - pre) then 
	{
	  SendEnergy(((100 * energy) / 105) - 10, dir)
	}
      else 
	{
	  Skip
	}
    }
  else if (energy > 100) then
    {
      dup := 1;
      Duplicate(50, dir, chenille(dir, 0, num + 1)(pre, pas))
    }
  else
    { 
      Skip
    }
;;

@chenille(N, 0, 0)(10, 10)
