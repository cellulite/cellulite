serpent : vars (dir, pasRestants, b) args (cote, sens)
  if b = 0 then (* Duplication *)
    {
      b := 1;
      if See() band dir then (* Quelqu'un devant *)
	{
	  SendEnergy(energy / 2, dir)
	}
      else (* Personne devant *)
	{
	  if energy > costDuplicate(50, serpent) then
	    {
	      if pasRestants = 1 then (* On arrive devant un coin *)
		{
		  if sens = 1 then (* sens horaire *)
		    {
		      Duplicate(50, dir, serpent(NextDir(NextDir(dir)), cote - 1, 1)(cote, sens))
		    }
		  else (* sens trigo *)
		    {
		      Duplicate(50, dir, serpent(PrevDir(PrevDir(dir)), cote - 1, 1)(cote, sens))
		    }
		}
	      else (* En ligne droite *)
		{
		  Duplicate(20, dir, serpent(dir, pasRestants - 1, 1)(cote, sens))
		}
	    }
	  else
	    {
	      Skip
	    }
	}
    }
(*  else if b = 1 then (* Déplacement *)
    {
      b := 2;
      if pasRestants = 1 then (* On va arriver dans un coin *)
	{
	  pasRestants := cote - 1;
	  if sens = 1 then (* sens horaire *)
	    {
	      let tmp = dir in
		dir := NextDir(NextDir(dir));
		Move(tmp)
	    }
	  else (* sens trigo *)
	    {
	      let tmp = dir in
		dir := PrevDir(PrevDir(dir));
		Move(tmp)
	    }
	}
      else (* Milieu d'une ligne droite *)
	{
	  pasRestants := pasRestants - 1;
	  Move(dir)
	}
    }
*)  else (* b > 1 : On accumule de l'énegie *)
    {
      b := (b + 1) % 20;
      Skip
    }
;;
@serpent(N, 5, 0)(10, 1)

