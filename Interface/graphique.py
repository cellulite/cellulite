from Tkinter import *
import Tkinter
from processus_reseau import *
import lecture
from lecture import *

Width_of_canvas = 700 #Largeur max : 70
Height_of_canvas = 400 #Hauteur max : 40
Haut_cell = 50
Larg_cell = 100
MAX_X_SIZE = 10000 # Largeur max = 100
MAX_Y_SIZE = 10000 # Longueur max = 100
identifiant = "undefinied"
mot_de_passe = "undefinied"
      


def disconnect():
    global identifiant, log_in, mdp, mot_de_passe, root, bouton_connexion, canevas, Width_of_canvas, Height_of_canvas, sending_butt, sending_entry, inscript_butt, reponse, error_network, error_login, deleting_butt, deleting_entry
    sending_entry.pack_forget()
    sending_butt.pack_forget()
    deleting_butt.pack_forget()
    deleting_entry.pack_forget()
    canevas.pack_forget()
    reponse.pack_forget()           
    deco_butt.pack_forget()
    log_in.pack()
    mdp.pack()
    kill_process()
    bouton_connexion.pack()
    inscrip_butt.pack()


def print_score(tableau, max_line_before, max_col):
    global canevas, Larg_cell, Haut_cell, global_id, identifiant
    max_line = max_line_before+1
    canevas.create_line(1, 1, max_col*Larg_cell, 1)
    canevas.create_line(1, 1, 1, max_line*Haut_cell)
    canevas.create_line(max_col*Larg_cell, 1, max_col*Larg_cell, max_line*Haut_cell)
    canevas.create_line(1, max_line*Haut_cell, max_col*Larg_cell, max_line*Haut_cell)
    for i in range(max_col) :
        canevas.create_line(i*Larg_cell, 1, i*Larg_cell, (Haut_cell) * max_line)
        if(i==0):
            canevas.create_text(i*Larg_cell+25, 25, text="id", fill="blue")
        else:
            canevas.create_text(i*Larg_cell+50, 25,text="famille "+str(i), fill="blue")
    for i in range(max_line-1) :
        canevas.create_line(1, (i+1)*Haut_cell, max_col*Larg_cell, (i+1)*Haut_cell)
        if(tableau[i][0] == identifiant):
            canevas.create_text(25, (i+1)*Haut_cell+25, text=tableau[i][0], fill="dark green")
            for j in range(max_col-1) :
                canevas.create_text((j+1)*Larg_cell+50, (i+1)*Haut_cell+25, text=tableau[i][j+1], fill="dark green")
        else:
            canevas.create_text(25, (i+1)*Haut_cell+25, text=tableau[i][0], fill="red")
            for j in range(max_col-1) :
                canevas.create_text((j+1)*Larg_cell+50, (i+1)*Haut_cell+25, text=tableau[i][j+1], fill="red")


def maj():
    global canevas, error_network, error_login
    tab = []
    tab = read_score()
    if(tab == -2):
        error_login.pack(side = TOP)
        disconnect()
        return
    elif(tab == -1):
        error_network.pack(side = TOP)
        disconnect()
        return
    else :
        error_network.pack_forget()
        error_login.pack_forget()
    canevas.delete(ALL)
    if(len(tab) > 0):
        print_score(tab, len(tab), len(tab[0]))
    root.after(10, maj)


def subscribe():
    global newWindow, entry_pass, entry_log, label_allready_use, label_connection_failed
    log_to_send = entry_log.get()
    pass_to_send = entry_pass.get()
    return_value = send_subscribe(log_to_send, pass_to_send)
    if(return_value == 0):
        newWindow.destroy()
    else:
        if(return_value == -2):
            label_allready_use.pack()
        else :
            label_connection_failed.pack()
        
def open_subscribe_window():
    global newWindow, entry_pass, entry_log, label_allready_use, label_connection_failed
    newWindow=Toplevel()
    newWindow.grab_set()
    newWindow.focus_set()
    newWindow.geometry("400x400")
    submit_butt = Tkinter.Button(newWindow, text="Soumettre", command=subscribe)
    entry_pass = Tkinter.Entry(newWindow, show='*')
    entry_log = Tkinter.Entry(newWindow)
    label_allready_use = Tkinter.Label(newWindow, text="Echec : identifiant existant")
    label_connection_failed = Tkinter.Label(newWindow, text="Echec : la connexion a echoue")
    entry_log.pack()
    entry_pass.pack()
    submit_butt.pack()
    
def exit_game():
    global root
    kill_process()
    root.quit()

def initialisation_graphique():
    global root, history_frame, log_in, mdp, bouton_connexion, canevas, Width_of_canvas, Height_of_canvas, sending_butt, sending_entry, error_lab, error_network, error_login, inscrip_butt, deco_butt, deleting_butt, deleting_entry
    root = Tkinter.Tk()
    root.geometry("800x800")
    root.title('Grain de Cell')
    canevas = Tkinter.Canvas(root, width = Width_of_canvas, height = Height_of_canvas, bg='grey')

    deleting_entry = Entry()
    deleting_butt = Tkinter.Button(root, text="Supprimer une famille", command=delete_cell)
    sending_entry = Entry()
    sending_butt = Tkinter.Button(root, text="Envoyer une cellule", command=send_cell)
    inscrip_butt = Tkinter.Button(root, text="Inscription", command=open_subscribe_window)
    deco_butt = Tkinter.Button(root, text="Deconnexion", command=disconnect)
    scroll_down = Tkinter.Scrollbar(root, orient = HORIZONTAL)
    scroll_right = Tkinter.Scrollbar(root, orient = VERTICAL)
    scroll_right.config(command = canevas.yview)
    scroll_down.config(command = canevas.xview)
    canevas.config(scrollregion = (0,0, MAX_X_SIZE, MAX_Y_SIZE))
    canevas.config(yscrollcommand = scroll_right.set)
    canevas.config(xscrollcommand = scroll_down.set)
    scroll_right.pack(side = RIGHT, fill=Tkinter.Y)
    scroll_down.pack(side = BOTTOM, fill=Tkinter.X)
    
    log_in = Tkinter.Entry()
    mdp = Tkinter.Entry(show="*")
    log_in.pack()
    mdp.pack()
    error_lab = Tkinter.Label(root, text="Sending troubles : check the file and/or refer to the compile_report.txt file")
    error_network = Tkinter.Label(root, text="Connection Failed : check your network connection")
    error_login = Tkinter.Label(root, text="Connection Failed : Bad login/password, please register")

    bouton_connexion = Tkinter.Button(root, text="Connexion", command=logging)
    bouton_connexion.pack()
    inscrip_butt.pack()

    leave_butt = Tkinter.Button(root, text="Quitter", command=exit_game)
    leave_butt.pack(side = BOTTOM)


def send_cell():
    global identifiant, log_in, mdp, mot_de_passe, root, bouton_connexion, canevas, Width_of_canvas, Height_of_canvas, sending_butt, sending_entry, error_lab
    file_name = sending_entry.get()
    sending_entry.delete(0, Tkinter.END)
    processus_cell(file_name, identifiant, mot_de_passe)
    
        

def delete_cell():
    global identifiant, log_in, mdp, mot_de_passe, root, bouton_connexion, canevas, Width_of_canvas, Height_of_canvas, sending_butt, sending_entry, error_lab
    num_cell = deleting_entry.get()
    if(num_cell != ""):
        deleting_entry.delete(0,Tkinter.END)
        processus_del(num_cell, identifiant, mot_de_passe)

    
def logging():
    global identifiant, log_in, mdp, mot_de_passe, root, bouton_connexion, canevas, Width_of_canvas, Height_of_canvas, sending_butt, sending_entry, inscript_butt, reponse, deco_butt, global_id
    identifiant = log_in.get()
    mot_de_passe = mdp.get()
    if(mot_de_passe == "" or identifiant == "") :
        return
    global_id = init_receiving(identifiant, mot_de_passe)
    if(global_id > 0):
           reponse = Tkinter.Label(root, text="Bonjour "+identifiant)
           reponse.pack(side=TOP)
           mdp.pack_forget()
           log_in.pack_forget()
           error_network.pack_forget()
           error_login.pack_forget()
           inscrip_butt.pack_forget()
           bouton_connexion.pack_forget()
           deleting_entry.pack(side = TOP)
           deleting_butt.pack(side = TOP)
           sending_entry.pack(side = TOP)
           sending_butt.pack(side = TOP)
           canevas.pack(side = BOTTOM, expand = YES)
           canevas.create_line(2,2,2,Height_of_canvas-2, fill="black")
           canevas.create_line(2,2,Width_of_canvas-2,2, fill="black")
           canevas.create_line(2,Height_of_canvas-2,Width_of_canvas-2,Height_of_canvas-2, fill="black")
           canevas.create_line(Width_of_canvas-2,2,Width_of_canvas-2,Height_of_canvas-2, fill="black")
           deco_butt.pack(side=BOTTOM)
           maj()



def main():
    global root, history_frame, log_in, mdp, bouton_connexion, canevas, Width_of_canvas, Height_of_canvas, sending_butt, sending_entry, cell_x, cell_y, deleting_butt, deleting_entry

    initialisation_graphique()

    root.mainloop()


main()
kill_process()
